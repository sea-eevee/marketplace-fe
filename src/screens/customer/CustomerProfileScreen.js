import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Footer from "../../components/Footer";
import { Container, Card, Form, Button } from "react-bootstrap";
import CustomerNavbar from "../../components/customer/CustomerNavbar";
import SideProfile from "../../components/SideProfile";
import CustomerProfileTab from "../../components/customer/CustomerProfileTab";
import { Redirect } from "react-router-dom";
import {
  getUserProfile,
  updateUserProfile,
} from "../../actions/customerActions";

function CustomerProfileScreen(props) {
  const [displayName, setDisplayName] = useState("");
  const [address, setAddress] = useState("");

  const dispatch = useDispatch();
  const userSignin = useSelector((state) => state.userSignin);
  const { userInfo } = userSignin;

  const userProfile = useSelector((state) => state.userProfile);
  const { customer_profile, loading } = userProfile;

  const userProfileUpdate = useSelector((state) => state.userProfileUpdate);
  const { customer_profile: updatedProfile } = userProfileUpdate;

  useEffect(() => {
    if (userInfo) {
      dispatch(getUserProfile(userInfo.user_id));
    }
    // eslint-disable-next-line
  }, [updatedProfile]);

  useEffect(() => {
    setDisplayName(customer_profile.display_name);
    setAddress(customer_profile.address);
    // eslint-disable-next-line
  }, [loading]);

  const profileSubmitHandler = (e) => {
    e.preventDefault();
    if (userInfo) {
      dispatch(updateUserProfile(userInfo.user_id, displayName, address));
    }
  };

  return userInfo ? (
    <div className="grid-container">
      <header className="header bg-light">
        <CustomerNavbar
          customerName={userInfo.username}
          parentHistory={props.history}
        />
      </header>
      <main className="main bg-light" style={{ minHeight: "80vh" }}>
        <Container className="d-flex py-5">
          <SideProfile data={customer_profile}>
            <Button
              href="/customer/order"
              variant="warning"
              className="font-weight-bolder"
            >
              Pending Order List
            </Button>
          </SideProfile>
          <div className="flex-fill d-flex flex-column">
            <CustomerProfileTab defaultActiveKey="link-1" />
            <Card>
              <Card.Body>
                <Form onSubmit={profileSubmitHandler}>
                  <Form.Group controlId="formGridName">
                    <Form.Label>Name</Form.Label>
                    <Form.Control
                      placeholder="Your Name"
                      defaultValue={customer_profile.display_name}
                      onChange={(e) => setDisplayName(e.target.value)}
                    />
                  </Form.Group>

                  <Form.Group controlId="formGridAddress">
                    <Form.Label>Address</Form.Label>
                    <Form.Control
                      placeholder="Gedung Blok Z, Jl. Angrek no. 3, Rt04 / Rw09"
                      defaultValue={customer_profile.address}
                      onChange={(e) => setAddress(e.target.value)}
                    />
                  </Form.Group>

                  <Form.Group id="formGridCheckbox">
                    <Form.Check type="checkbox" label="Check me out" />
                  </Form.Group>

                  <Button variant="info" type="submit">
                    Update
                  </Button>
                </Form>
              </Card.Body>
            </Card>
          </div>
        </Container>
      </main>
      <Footer />
    </div>
  ) : (
    <Redirect
      to={{
        pathname: "/customer/login",
        state: {
          from: props.location,
        },
      }}
    />
  );
}

export default CustomerProfileScreen;
