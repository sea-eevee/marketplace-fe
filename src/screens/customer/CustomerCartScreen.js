import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Footer from "../../components/Footer";
import { Container, Card, Button, Modal } from "react-bootstrap";
import CustomerNavbar from "../../components/customer/CustomerNavbar";
import { Redirect } from "react-router-dom";
import { cartEditQuantity, userCartOrder, userReadCart } from "../../actions/customerActions";

function CustomerCartScreen(props) {
  const dispacth = useDispatch();

  const userSignin = useSelector((state) => state.userSignin);
  const { userInfo } = userSignin;

  const readCart = useSelector((state) => state.userReadCart);
  const { items, loading, error } = readCart;

  useEffect(() => {
    if (userInfo) {
      dispacth(userReadCart(userInfo.user_id));
    }
    // eslint-disable-next-line
  }, []);

  function sumItems(items) {
    let sumQuantity = 0;
    let sumPrice = 0;
    if (items != null) {
      items.forEach((item) => {
        sumQuantity = sumQuantity + item.quantity;
        sumPrice = sumPrice + item.price * item.quantity;
      });
    }

    return { sumQuantity: sumQuantity, sumPrice: sumPrice };
  }

  const { sumQuantity, sumPrice } = sumItems(items);

  const addQuantity = (item) => {
    item.quantity = item.quantity + 1;
    if (userInfo) {
      dispacth(cartEditQuantity(userInfo.user_id, item.id, true));
    }
    setTrigger(!trigger);
  };

  const minusQuantity = (item) => {
    if (item.quantity > 1) {
      item.quantity = item.quantity - 1;
      if (userInfo) {
        dispacth(cartEditQuantity(userInfo.user_id, item.id, false));
      }
      setTrigger(!trigger);
    }
  };

  const deleteItem = (item) => {
    deleteHandleClose();
    if (userInfo) {
      for (let i = 0; i < item.quantity; i++) {
        dispacth(cartEditQuantity(userInfo.user_id, item.id, false));
      }
    }
    const index = items.indexOf(item);
    items.splice(index, 1);
    setTrigger(!trigger);
  };

  //Modal delete function
  const [deleteShow, setDeleteShow] = useState(false);
  const deleteHandleClose = () => setDeleteShow(false);
  const deleteHandleShow = (item) => {
    setDeleteShow(true);
    setRefItem(item);
  };
  const [refItem, setRefItem] = useState({});

  //trigger for reaload component
  const [trigger, setTrigger] = useState(false);

  //order function
  const orderCheckout = () => {
    if(userInfo){
      dispacth(userCartOrder(userInfo.user_id));
      props.history.push("/customer/order");
    }
  }

  return userInfo ? (
    <div className="grid-container">
      <header className="header bg-light">
        <CustomerNavbar
          customerName={userInfo.username}
        />
      </header>
      <main className="main bg-light" style={{ minHeight: "80vh" }}>
        {items == null ? (
          <div></div>
        ) : (
          <Container className="d-flex py-5" style={{ height: "100%" }}>
            <div className="d-flex flex-column flex-grow-1">
              {loading ? (
                <div>Loading..</div>
              ) : error ? (
                <div>{error}</div>
              ) : (
                items.map((item) => (
                  <Card key={item.id} className="my-1">
                    <Card.Body>
                      <Card.Link
                        href={`/customer/merchant/${item.merchant_id}`}
                        style={{ textDecoration: "none", color: "black" }}
                      >
                        <div>
                          <div className="font-weight-bolder">
                            Merchant ID: {item.merchant_id}
                          </div>
                        </div>
                      </Card.Link>
                      <div className="border-bottom border-secondary mb-3"></div>
                      <Card.Link
                        style={{ textDecoration: "none", color: "black" }}
                      >
                        <div className="ml-3 mb-5">
                          <div
                            className="font-weight-bolder"
                            style={{ fontSize: "1.2rem" }}
                          >
                            {item.display_name}
                          </div>
                          <div className="text-success font-weight-bolder">
                            Rp. {item.price.toLocaleString("id")}
                          </div>
                        </div>
                      </Card.Link>
                      <div className="d-flex justify-content-end mb-5">
                        <Card.Text className="font-weight-light mr-2">
                          Quantitiy:
                        </Card.Text>
                        <Button
                          variant="outline-secondary"
                          onClick={() => minusQuantity(item)}
                          className="font-weight-bold"
                        >
                          −
                        </Button>
                        <div
                          className="mx-2 align-self-center text-center"
                          style={{ width: "3rem" }}
                        >
                          {item.quantity}
                        </div>
                        <Button
                          variant="outline-secondary"
                          onClick={() => addQuantity(item)}
                          className="font-weight-bold"
                        >
                          +
                        </Button>
                      </div>
                      <div className="d-flex mb-5">
                        <Button
                          variant="danger"
                          onClick={() => deleteHandleShow(item)}
                          className="font-weight-bolder"
                          style={{ width: "6rem" }}
                        >
                          Delete
                        </Button>
                      </div>
                    </Card.Body>
                  </Card>
                ))
              )}
            </div>
            <div className="d-flex flex-column">
              <Card style={{ width: "18rem", margin: "2rem" }}>
                <Card.Body>
                  <div className="d-flex flex-column">
                    <Card.Title>Shopping summary</Card.Title>
                    <div className="d-flex justify-content-between">
                      <Card.Text>Total Price</Card.Text>
                      <Card.Text className="text-success font-weight-bolder">
                        Rp. {sumPrice.toLocaleString("id")}
                      </Card.Text>
                    </div>
                    <Button
                      variant="success"
                      onClick={orderCheckout}
                      className="font-weight-bold"
                    >
                      Order ({sumQuantity})
                    </Button>
                  </div>
                </Card.Body>
              </Card>
            </div>
          </Container>
        )}

        {/* modal for delete card item */}
        <Modal
          show={deleteShow}
          onHide={deleteHandleClose}
          centered
          dialogClassName="text-center"
        >
          <Modal.Body>
            <Modal.Title>Remove an Item?</Modal.Title>
            <p className="font-weight-light">
              This item will be removed from your cart.
            </p>
            <div className="d-flex justify-content-center">
              <Button
                variant="outline-info"
                onClick={deleteHandleClose}
                style={{ width: "8rem" }}
                className="mr-3 font-weight-bolder"
              >
                Cancel
              </Button>
              <Button
                variant="info"
                onClick={() => deleteItem(refItem)}
                style={{ width: "8rem" }}
                className="font-weight-bolder"
              >
                Remove Item
              </Button>
            </div>
          </Modal.Body>
        </Modal>
      </main>
      <Footer />
    </div>
  ) : (
    <Redirect
      to={{
        pathname: "/customer/login",
        state: {
          from: props.location,
        },
      }}
    />
  );
}
export default CustomerCartScreen;
