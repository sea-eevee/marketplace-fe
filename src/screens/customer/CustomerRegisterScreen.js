import React, { useEffect, useState } from "react";
import Eevee from "../../images/eevee.svg";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { register } from "../../actions/customerActions";
import BrandNavbar from "../../components/BrandNavbar";
import Footer from "../../components/Footer";
import { Card, Button, Container, Form } from "react-bootstrap";

function CustomerRegisterScreen(props) {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [rePassword, setRePassword] = useState("");
  const userRegister = useSelector((state) => state.userRegister);
  const { loading, userInfo, error } = userRegister;
  const dispatch = useDispatch();

  useEffect(() => {
    if (userInfo) {
      props.history.push("/");
    }
    return () => {};
    // eslint-disable-next-line
  }, [userInfo]);

  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(register(name, email, password === rePassword && password));
  };

  return (
    <div className="grid-container">
      <header className="header">
      <BrandNavbar />
      </header>
      <main className="main" style={{minHeight: "80vh"}}>
      <Container
        fluid
        className="d-flex align-items-center justify-content-center py-5"
      >
        <img
          alt=""
          src={Eevee}
          width="600"
          height="600"
          className="d-none d-lg-block"
        />
        <Card className="text-center">
          <Card.Header>Register</Card.Header>
          <Card.Body>
            <Form onSubmit={submitHandler}>
              <Form.Group
                controlId="formBasicName"
                className="d-flex flex-column align-items-start"
              >
                <Form.Label>Username</Form.Label>
                <Form.Control
                  placeholder="Enter unique username"
                  onChange={(e) => setName(e.target.value)}
                />
              </Form.Group>
              <Form.Group
                controlId="formBasicEmail"
                className="d-flex flex-column align-items-start"
              >
                <Form.Label>Email Address</Form.Label>
                <Form.Control
                  type="email"
                  placeholder="Enter email"
                  onChange={(e) => setEmail(e.target.value)}
                />
                <Form.Text className="text-muted">
                  We'll never share your email with anyone else.
                </Form.Text>
              </Form.Group>
              <Form.Group
                controlId="formBasicPassword"
                className="d-flex flex-column align-items-start"
              >
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Password"
                  onChange={(e) => setPassword(e.target.value)}
                />
              </Form.Group>
              <Form.Group
                controlId="formBasicRePassword"
                className="d-flex flex-column align-items-start"
              >
                <Form.Label>Re-Enter Password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Confirm Password"
                  onChange={(e) => setRePassword(e.target.value)}
                />
              </Form.Group>
              <Button variant="info" type="submit">
                Submit
              </Button>
            </Form>
            {loading && <Card.Text>Loading..</Card.Text>}
            {error && <Card.Text>{error}</Card.Text>}
          </Card.Body>
          <Card.Footer>
            <p>Already have an account?</p>
            <Link to="/customer/login">Sign In</Link>
          </Card.Footer>
        </Card>
      </Container>
      </main>
      <Footer />
    </div>
  );
}

export default CustomerRegisterScreen;
