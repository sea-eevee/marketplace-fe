import React, { useEffect, useState } from "react";
import Footer from "../../components/Footer";
import CustomerNavbar from "../../components/customer/CustomerNavbar";
import { Container, Card, Button, Modal } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import ItemBg from "../../blank-item-colored.jpg";
import HomeNavbar from "../../components/HomeNavbar";
import { userAddCart } from "../../actions/customerActions";

function CustomerItemScreen(props) {
  const dispatch = useDispatch();
  const [quantity, setQuantity] = useState(1);
  const [cartItem, setCartItem] = useState({});

  const addQuantity = () => {
    setQuantity(quantity + 1);
  };

  const dscQuantity = () => {
    if (quantity > 1) {
      setQuantity(quantity - 1);
    }
  };

  useEffect(() => {
    setCartItem({
      id: item.id,
      quantity: quantity
    });
    // eslint-disable-next-line
  }, [quantity]);

  const selector = (state) => {
    if (state.userSignin.userInfo) {
      return state.userSignin;
    } else {
      return state.userRegister;
    }
  };

  const userSignin = useSelector((state) => selector(state));
  const { userInfo } = userSignin;

  const userDetailMerchant = useSelector((state) => state.userDetailMerchant);
  const {data} = userDetailMerchant;
  const item = data.items[props.match.params.id];

  const addCart = () => {
    console.log("hello");
    console.log(cartItem);
    console.log(item);
    if(userInfo){
      dispatch(userAddCart(userInfo.user_id, cartItem));
      handleShow();
    }
  }

  //Modal function
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <div className="grid-container">
      <header className="header bg-light">
        {userInfo ? (
          <CustomerNavbar
            customerName={userInfo.username}
            parentHistory={props.history}
          />
        ) : (
          <HomeNavbar />
        )}
      </header>
      <main className="main bg-light" style={{minHeight: "80vh"}}>
        <Container className="py-5" style={{ height: "100%" }}>
          <Card style={{ minHeight: "100%", width: "100%" }}>
            <Card.Body>
              <div className="d-flex">
                <Card.Img
                  src={ItemBg}
                  className="bg-info"
                  style={{ width: "40%" }}
                />
                <div
                  className="d-flex flex-column justify-content-between p-3"
                  style={{ width: "100%" }}
                >
                  <div className="d-flex flex-column">
                    <Card.Title>
                    {item.display_name}
                    </Card.Title>
                    <Card.Text
                      className="text-success font-weight-bolder"
                      style={{ fontSize: "1.5rem" }}
                    >
                     Rp. {item.price.toLocaleString("id")}
                    </Card.Text>
                    <Card.Text className="font-weight-light">
                      Stock: {item.quantity}
                    </Card.Text>
                    <div className="border-bottom border-secondary mb-3"></div>
                  </div>
                  <div className="d-flex flex-column">
                    <div className="d-flex mb-3">
                      <Card.Text className="font-weight-light mr-2">
                        Quantity:
                      </Card.Text>
                      <Button
                        variant="outline-secondary"
                        onClick={dscQuantity}
                        className="font-weight-bold"
                      >
                        −
                      </Button>
                      <div
                        className="mx-2 align-self-center text-center"
                        style={{ width: "3rem" }}
                      >
                        {quantity}
                      </div>
                      <Button
                        variant="outline-secondary"
                        onClick={addQuantity}
                        className="font-weight-bold"
                      >
                        +
                      </Button>
                    </div>
                    <div className="d-flex">
                      <Button
                        variant="outline-success"
                        onClick={addCart}
                        className="font-weight-bolder mr-3"
                        style={{ width: "8rem" }}
                      >
                        Add to Cart
                      </Button>
                    </div>
                  </div>
                </div>
              </div>
            </Card.Body>
          </Card>
        </Container>

        {/* Modal for notify added to cart */}
        <Modal
          show={show}
          onHide={handleClose}
          centered
          className="text-center"
        >
          <Modal.Body>
            <Modal.Header closeButton>
              <Modal.Title>Successfully Added</Modal.Title>
            </Modal.Header>
            <p className="font-weight-light">
              {item.display_name}
            </p>
            <div className="d-flex justify-content-end">
              <Button
                href="/customer/cart"
                variant="info"
                style={{ width: "8rem" }}
                className="font-weight-bolder"
              >
                View Cart
              </Button>
            </div>
          </Modal.Body>
        </Modal>
      </main>
      <Footer />
    </div>
  );
}

export default CustomerItemScreen;
