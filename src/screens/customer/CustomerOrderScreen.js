import React, { useEffect, useState } from "react";
import { Button, Card, Container, Form, Modal } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { Redirect } from "react-router-dom";
import { getUserListOrder, userPaidOrder } from "../../actions/customerActions";
import CustomerNavbar from "../../components/customer/CustomerNavbar";
import Footer from "../../components/Footer";

function CustomerOrderScreen(props) {
  const dispatch = useDispatch();
  const userSignin = useSelector((state) => state.userSignin);
  const { userInfo } = userSignin;

  const userListOrder = useSelector((state) => state.userListOrder);
  const { customer_order, loading, error } = userListOrder;

  useEffect(() => {
    if (userInfo) {
      dispatch(getUserListOrder(userInfo.user_id));
    }
    // eslint-disable-next-line
  }, []);

  //Modal paid function
  const [paidShow, setPaidShow] = useState(false);
  const paidHandleClose = () => setPaidShow(false);
  const paidHandleShow = (order) =>{
    setPaidShow(true);
    setRefOrder(order);
  };

  const [refOrder, setRefOrder] = useState({});

  const [bankName, setBankName] = useState("");
  const [bankNumber, setBankNumber] = useState("");

  const paidOrder = (order) => {
    paidHandleClose();
    if(userInfo){
      dispatch(userPaidOrder(userInfo.id, {id: order.order_id, bank_name: bankName, bank_number: bankNumber}));
      props.history.push("/customer/history");
    }
  }

  return userInfo ? (
    <div className="grid-container">
      <header className="header bg-light">
        <CustomerNavbar customerName={userInfo.username} />
      </header>
      <main className="main bg-light" style={{ minHeight: "80vh" }}>
        <Container
          className="d-flex flex-column py-5"
          style={{ height: "100%" }}
        >
          {loading ? (
            <div>Loading..</div>
          ) : error ? (
            <div>{error}</div>
          ) : (
            customer_order
              .filter((order) => !order.is_paid)
              .map((item) => (
                <Card key={item.order_details[0].order_id} className="my-1">
                  <Card.Body>
                    <Card.Link
                      style={{ textDecoration: "none", color: "black" }}
                    >
                      <div className="d-flex justify-content-between">
                        <div className="font-weight-bolder">
                          Order ID: {item.order_id}
                        </div>
                        <div className="font-weight-light">
                          {new Date(item.created_at).toTimeString()}
                        </div>
                      </div>
                    </Card.Link>
                    <div className="border-bottom border-secondary mb-3"></div>
                    <Card.Link
                      style={{ textDecoration: "none", color: "black" }}
                    >
                      <div className="ml-3 mb-5">
                        <div
                          className="font-weight-bolder"
                          style={{ fontSize: "1.2rem" }}
                        >
                          {item.order_details[0].item.display_name}
                        </div>
                        <div className="text-success font-weight-bolder">
                          Rp.{" "}
                          {item.order_details[0].item.price.toLocaleString(
                            "id"
                          )}
                        </div>
                      </div>
                    </Card.Link>
                    <div className="d-flex justify-content-end mb-3">
                      <div className="d-flex flex-column mr-5">
                        <div className="d-flex">
                          <Card.Text className="mr-2">Quantitiy:</Card.Text>
                          <div className="text-secondary font-weight-bolder mx-2">
                            {200}
                          </div>
                        </div>
                        <div className="text-secondary font-weight-bolder">
                          Total
                        </div>
                        <div className="text-success font-weight-bolder">
                          Rp.{" "}
                          {(
                            item.order_details[0].item.price *
                            item.order_details[0].quantity
                          ).toLocaleString("id")}
                        </div>
                        <Button
                          size="sm"
                          variant="success"
                          onClick={() => paidHandleShow(item)}
                          className="font-weight-bolder mt-5"
                        >
                          Paid
                        </Button>
                      </div>
                    </div>
                  </Card.Body>
                </Card>
              ))
          )}
        </Container>

        {/* modal for paid item */}
        <Modal
          show={paidShow}
          onHide={paidHandleClose}
          centered
          dialogClassName="text-center"
        >
          <Modal.Body>
            <Modal.Title>Enter your bank account</Modal.Title>
            <div className="d-flex p-5">
              <Form className="flex-grow-1 text-left">
                <Form.Group controlId="formGridName">
                  <Form.Label>Bank Name</Form.Label>
                  <Form.Control placeholder="Your Bank Name"  onChange={(e) => setBankName(e.target.value)} />
                </Form.Group>

                <Form.Group controlId="formGridNumber">
                  <Form.Label>Bank Number</Form.Label>
                  <Form.Control placeholder="Your Bank account Number" onChange={(e) => setBankNumber(e.target.value)} />
                </Form.Group>

                <Form.Group id="formGridCheckbox">
                  <Form.Check type="checkbox" label="Check me out" />
                </Form.Group>
                <div className="d-flex justify-content-center">
                  <Button
                    variant="outline-info"
                    onClick={paidHandleClose}
                    style={{ width: "8rem" }}
                    className="mr-3 font-weight-bolder"
                  >
                    Cancel
                  </Button>
                  <Button
                    variant="info"
                    onClick={() => paidOrder(refOrder)}
                    style={{ width: "8rem" }}
                    className="font-weight-bolder"
                  >
                    Paid
                  </Button>
                </div>
              </Form>
            </div>
          </Modal.Body>
        </Modal>
      </main>
      <Footer />
    </div>
  ) : (
    <Redirect
      to={{
        pathname: "/customer/login",
        state: {
          from: props.location,
        },
      }}
    />
  );
}
export default CustomerOrderScreen;
