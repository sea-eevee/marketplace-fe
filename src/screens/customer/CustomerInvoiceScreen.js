import React from "react";
import Footer from "../../components/Footer";
import CustomerNavbar from "../../components/customer/CustomerNavbar";
import { useSelector } from "react-redux";
import { Container, Card } from "react-bootstrap";
import { Redirect } from "react-router-dom";

function CustomerInvoiceScreen(props) {
  const userSignin = useSelector((state) => state.userSignin);
  const { userInfo } = userSignin;

  return userInfo ? (
    <div className="grid-container">
      <header className="header bg-light">
        <CustomerNavbar
          customerName={userInfo.username}
          parentHistory={props.history}
        />
      </header>
      <main className="main bg-light" style={{minHeight: "80vh"}}>
        <Container style={{ height: "100%" }}>
          <Card style={{ height: "90%" }}>
            <Card.Body className="d-flex flex-column align-items-center">
              <Card.Title>Payment Invoice</Card.Title>
              <div className="text-secondary">issue date</div>
              <div className="text-secondary font-weight-bolder">
                11 September 2020 15:32
              </div>
              <Card
                border="secondary"
                className="mt-5"
                style={{ width: "60%" }}
              >
                <Card.Body className="p-3">
                  <div className="text-secondary font-weight-bolder text-left mb-3">
                    Awaiting Payment transfer
                  </div>
                  <div className="border-bottom border-secondary mb-3"></div>
                  <div className="d-flex justify-content-between mx-5 mb-3">
                    <div className="d-flex flex-column">
                      <div className="text-secondary">Your bank account</div>
                      <div>
                        BNI
                        <div className="text-secondary font-weight-bolder">
                          5176438576874910
                        </div>
                      </div>
                    </div>
                    <div className="d-flex flex-column">
                      <div className="text-secondary">recipient account</div>
                      <div>
                        BCA
                        <div className="text-secondary font-weight-bolder">
                          5229909922950312
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="text-left">
                    <div className="text-secondary">Total Payment</div>
                    <div className="text-success font-weight-bolder">
                      Rp. 200.000
                    </div>
                  </div>
                </Card.Body>
              </Card>
            </Card.Body>
          </Card>
        </Container>
      </main>
      <Footer />
    </div>
  ) : (
    <Redirect
      to={{
        pathname: "/customer/login",
        state: {
          from: props.location,
        },
      }}
    />
  );
}

export default CustomerInvoiceScreen;
