import React, { useEffect } from "react";
import { Container, Card } from "react-bootstrap";
import CustomerNavbar from "../../components/customer/CustomerNavbar";
import { useDispatch, useSelector } from "react-redux";
import MerchantBg from "../../blank-bg-colored.jpg";
import ItemBg from "../../blank-item-colored.jpg";
import Footer from "../../components/Footer";
import HomeNavbar from "../../components/HomeNavbar";
import { getDetailMerchant } from "../../actions/customerActions";
import { Link } from "react-router-dom";

function CustomerMerchantScreen(props) {
  const dispatch = useDispatch();

  const selector = (state) => {
    if (state.userSignin.userInfo) {
      return state.userSignin;
    } else {
      return state.userRegister;
    }
  };

  const userSignin = useSelector((state) => selector(state));
  const { userInfo } = userSignin;

  const userDetailMerchant = useSelector((state) => state.userDetailMerchant);
  const { data, loading, error } = userDetailMerchant;

  useEffect(() => {
    dispatch(getDetailMerchant(props.match.params.id));
    return () => {};
    // eslint-disable-next-line
  }, []);

  return (
    <div className="grid-container">
      <header className="header bg-light">
        {userInfo ? (
          <CustomerNavbar
            customerName={userInfo.username}
            parentHistory={props.history}
          />
        ) : (
          <HomeNavbar />
        )}
      </header>
      <main className="main bg-light" style={{minHeight: "80vh"}}>
        <Container className="d-flex flex-column">
          <Card>
            <Card.Img variant="top" src={MerchantBg} />
            <Card.Body>
              {loading ? (
                <div>Loading..</div>
              ) : error ? (
                <div>{error}</div>
              ) : (
                <div className="d-flex justify-content-between">
                  <div className="d-flex flex-column">
                    <Card.Title>{data.merchant.display_name}</Card.Title>
                    <Card.Text>
                      {data.merchant.location.city},{" "}
                      {data.merchant.location.province}
                    </Card.Text>
                  </div>
                </div>
              )}
            </Card.Body>
          </Card>
          <div className="d-flex flex-wrap justify-content-around">
            {data.items.map((item) => (
              <Card
                className="text-center my-5 eeveecard"
                style={{ width: "20rem" }}
                key={item.id}
              >
                  <Link to={`/customer/item/${data.items.indexOf(item)}`} style={{ color: "inherit", textDecoration: "inherit" }}>
                  <Card.Img variant="top" src={ItemBg} className="bg-info" />
                  <Card.Body>
                    <Card.Text className="text-secondary font-weight-bold">
                      {item.display_name}
                    </Card.Text>
                    <Card.Text className="text-success text-left font-weight-bolder mb-5">
                      Rp. {item.price.toLocaleString("id")}
                    </Card.Text>
                    <Card.Text className="text-left font-weight-light">
                      stock : {item.quantity.toLocaleString("id")}
                    </Card.Text>
                  </Card.Body>
                  </Link>
              </Card>
            ))}
          </div>
        </Container>
      </main>
      <Footer />
    </div>
  );
}

export default CustomerMerchantScreen;
