import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Footer from "../../components/Footer";
import { Container, Card, Button } from "react-bootstrap";
import CustomerNavbar from "../../components/customer/CustomerNavbar";
import CustomerProfileTab from "../../components/customer/CustomerProfileTab";
import SideProfile from "../../components/SideProfile";
import CustomerTransactionItem from "../../components/customer/CustomerTransactionItem";
import { Redirect } from "react-router-dom";
import {
  getUserProfile,
  userReadTransaction,
} from "../../actions/customerActions";

function CustomerTransactionScreen(props) {
  const dispatch = useDispatch();

  const userSignin = useSelector((state) => state.userSignin);
  const { userInfo } = userSignin;

  const userProfile = useSelector((state) => state.userProfile);
  const { customer_profile } = userProfile;

  const userTransaction = useSelector((state) => state.userTransaction);
  const {
    transactions,
    loading: loadingTransactionList,
    error: errorTransactionList,
  } = userTransaction;

  useEffect(() => {
    if (userInfo) {
      dispatch(getUserProfile(userInfo.user_id));
      dispatch(userReadTransaction(userInfo.user_id));
    }
    // eslint-disable-next-line
  }, []);

  return userInfo ? (
    <div className="grid-container">
      <header className="header bg-light">
        <CustomerNavbar
          customerName={userInfo.username}
          parentHistory={props.history}
        />
      </header>
      <main className="main bg-light" style={{ minHeight: "80vh" }}>
        <Container className="d-flex py-5">
          <SideProfile data={customer_profile}>
            <Button
              href="/customer/order"
              variant="warning"
              className="font-weight-bolder"
            >
              Pending Order List
            </Button>
          </SideProfile>
          <div className="flex-fill d-flex flex-column">
            <CustomerProfileTab defaultActiveKey="link-2" />
            <Card>
              <Card.Body>
                {loadingTransactionList ? (
                  <div>Loading..</div>
                ) : errorTransactionList ? (
                  <div>{errorTransactionList}</div>
                ) : (
                  transactions.map((transaction) => (
                    <CustomerTransactionItem
                      item={transaction}
                      key={transaction.transaction_id}
                    />
                  ))
                )}
              </Card.Body>
            </Card>
          </div>
        </Container>
      </main>
      <Footer />
    </div>
  ) : (
    <Redirect
      to={{
        pathname: "/customer/login",
        state: {
          from: props.location,
        },
      }}
    />
  );
}
export default CustomerTransactionScreen;
