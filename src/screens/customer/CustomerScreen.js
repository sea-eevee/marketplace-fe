import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import CustomerHomeScreen from "../../screens/customer/CustomerHomeScreen";
import CustomerSigninScreen from "../../screens/customer/CustomerSigninScreen";
import CustomerRegisterScreen from "../../screens/customer/CustomerRegisterScreen";
import CustomerProfileScreen from "../../screens/customer/CustomerProfileScreen";
import CustomerCartScreen from "../../screens/customer/CustomerCartScreen";
import CustomerTransactionScreen from "../../screens/customer/CustomerTransactionScreen";
import CustomerMerchantScreen from "./CustomerMerchantScreen";
import CustomerItemScreen from "./CustomerItemScreen";
import CustomerInvoiceScreen from "./CustomerInvoiceScreen";
import { Provider } from "react-redux";
import store from "../../stores/StoreCustomer";
import CustomerOrderScreen from "./CustomerOrderScreen";

function CustomerScreen(props) {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Route path="/" exact={true} component={CustomerHomeScreen} />

        <Route path="/customer/register" component={CustomerRegisterScreen} />
        <Route path="/customer/login" component={CustomerSigninScreen} />
        <Route path="/customer/profile" component={CustomerProfileScreen} />
        <Route path="/customer/cart" component={CustomerCartScreen} />
        <Route path="/customer/history" component={CustomerTransactionScreen} />
        <Route path="/customer/merchant/:id" component={CustomerMerchantScreen} />
        <Route path="/customer/item/:id" component={CustomerItemScreen} />
        <Route path="/customer/invoice" component={CustomerInvoiceScreen} />
        <Route path="/customer/order" component={CustomerOrderScreen} />
      </BrowserRouter>
    </Provider>
  );
}
export default CustomerScreen;
