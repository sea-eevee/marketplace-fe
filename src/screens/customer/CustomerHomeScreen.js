import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Container, Card, Button, Jumbotron } from "react-bootstrap";
import HomeNavbar from "../../components/HomeNavbar";
import CustomerNavbar from "../../components/customer/CustomerNavbar";
import Footer from "../../components/Footer";
import { LimitCharacters } from "../../Utils";
import MerchantBg from "../../sales.png";
import { getlistMerchant, getUserProfile } from "../../actions/customerActions";
import NotifProfile from "../../components/customer/NotifProfile";

function CustomerHomeScreen(props) {
  const dispatch = useDispatch();
  const selector = (state) => {
    if (state.userSignin.userInfo) {
      return state.userSignin;
    } else {
      return state.userRegister;
    }
  };
  const userSignin = useSelector((state) => selector(state));
  const { userInfo } = userSignin;

  const userListMerchant = useSelector((state) => state.userListMerchant);
  const { merchants, loading, error } = userListMerchant;

  const userProfile = useSelector((state) => state.userProfile);
  const { customer_profile, loadingProfile } = userProfile;

  useEffect(() => {
    dispatch(getlistMerchant());
    if (userInfo) {
      dispatch(getUserProfile(userInfo.user_id));
    }
    // eslint-disable-next-line
  }, []);

  return (
    <div className="grid-container">
      <header className="header bg-light">
        {userInfo ? (
          <CustomerNavbar
            customerName={userInfo.username}
            parentHistory={props.history}
          />
        ) : (
          <HomeNavbar />
        )}
      </header>
      <main className="main bg-light" style={{ minHeight: "80vh" }}>
        <Container className="d-flex flex-column">
          <Jumbotron className="bg-white">
            <h1>shop on our marketplace</h1>
            <p>
              Our marketplace provides a variety of items from various merchants
            </p>
          </Jumbotron>
          {loadingProfile ? (
            <div></div>
          ) : customer_profile.display_name === "blank" ||
            customer_profile.address === "blank" ? (
            <NotifProfile />
          ) : (
            <div></div>
          )}

          <div className="d-flex justify-content-around flex-wrap">
            {loading ? (
              <div>Loading..</div>
            ) : error ? (
              <div>{error}</div>
            ) : (
              merchants.map((merchant) => (
                <Card
                  className="rounded mb-5 text-center pb-5 eeveecard"
                  style={{ width: "20rem" }}
                  key={merchant.merchant_id}
                >
                  <Card.Img
                    variant="top"
                    src={MerchantBg}
                    className="bg-info"
                  />
                  <Card.Body>
                    <Card style={{ marginTop: "-20%", height: "6rem" }}>
                      <Card.Body>
                        <Card.Title>{merchant.display_name}</Card.Title>
                      </Card.Body>
                    </Card>
                    <Card.Text className="text-success">
                      {merchant.location.province}
                    </Card.Text>
                    <Card.Text className="font-weight-lighter">
                      {merchant.location.city}
                    </Card.Text>
                    <Card.Text className="mt-5" style={{ height: "7rem" }}>
                      {LimitCharacters(40, merchant.description)}
                    </Card.Text>
                    <Button
                      href={`/customer/merchant/${merchant.merchant_id}`}
                      variant="outline-info"
                    >
                      Shop Now
                    </Button>
                  </Card.Body>
                </Card>
              ))
            )}
          </div>
        </Container>
      </main>
      <Footer />
    </div>
  );
}
export default CustomerHomeScreen;
