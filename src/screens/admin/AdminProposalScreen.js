import React from 'react';
import BrandNavbar from '../../components/BrandNavbar';
import AdminHeader from '../../components/AdminHeader';
import AdminMenu from '../../components/AdminMenu';
import AdminProposal from '../../components/AdminProposal';
import Footer from '../../components/Footer';

function AdminProposalScreen(props) {
    return (
        <div>
        <BrandNavbar />
        <AdminHeader />
        <AdminMenu />
        <AdminProposal />
        <Footer />
        </div>
    );
  }
  
  export default AdminProposalScreen;