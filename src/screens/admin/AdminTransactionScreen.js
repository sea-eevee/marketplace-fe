import React from 'react';
import BrandNavbar from '../../components/BrandNavbar';
import AdminHeader from '../../components/AdminHeader';
import AdminMenu from '../../components/AdminMenu';
import AdminTransaction from '../../components/AdminTransaction';
import Footer from '../../components/Footer';

function AdminTransactionScreen(props) {
    return (
        <div>
        <BrandNavbar />
        <AdminHeader />
        <AdminMenu />
        <AdminTransaction />
        <Footer />
        </div>
    );
  }
  
  export default AdminTransactionScreen;