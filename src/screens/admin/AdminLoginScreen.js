import React, { useEffect, useState } from "react";
import Eevee from "../../images/eevee-admin.svg";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { signin } from "../../actions/adminActions";
import BrandNavbar from "../../components/BrandNavbar";
import Footer from "../../components/Footer";
import { Card, Button, Container, Form } from "react-bootstrap";

function AdminLoginScreen(props) {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const adminSignin = useSelector((state) => state.adminSignin);
  const { loading, adminInfo, error } = adminSignin;
  const dispatch = useDispatch();
  useEffect(() => {
    if (adminInfo) {
      props.history.push("/admin/dashboard");
    }
    return () => {};
  }, [adminInfo]);

  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(signin(username, password));
  };

  return (
    <div className="grid-container">
      <header className="header">
        <BrandNavbar />
      </header>
      <main className="main">
        <Container
          fluid
          className="d-flex align-items-center justify-content-center py-5"
        >
          <img
            alt=""
            src={Eevee}
            width="600"
            height="600"
            className="d-none d-lg-block"
          />
          <Card className="text-center">
            <Card.Header>Login as an Admin</Card.Header>
            <Card.Body>
              <Form onSubmit={submitHandler}>
                <Form.Group
                  controlId="formBasicName"
                  className="d-flex flex-column align-items-start"
                >
                  <Form.Label>Username</Form.Label>
                  <Form.Control
                    placeholder="Enter username"
                    onChange={(e) => setUsername(e.target.value)}
                  />
                </Form.Group>
                <Form.Group
                  controlId="formBasicPassword"
                  className="d-flex flex-column align-items-start"
                >
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    type="password"
                    placeholder="Password"
                    onChange={(e) => setPassword(e.target.value)}
                  />
                </Form.Group>
                <Button variant="info" type="submit">
                  Submit
                </Button>
              </Form>
              {loading && <Card.Text>Loading..</Card.Text>}
              {error && <Card.Text>{error}</Card.Text>}
            </Card.Body>
            <Card.Footer>
              <p>New to this?</p>
              <Link
                to="/admin/register"
                className="button secondary text-center"
              >
                Create your account
              </Link>
            </Card.Footer>
          </Card>
        </Container>
      </main>
      <Footer />
    </div>
  );
}

export default AdminLoginScreen;