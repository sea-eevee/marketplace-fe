import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import AdminRegisterScreen from "../../screens/admin/AdminRegisterScreen";
import AdminLoginScreen from "../../screens/admin/AdminLoginScreen";
import AdminDashboardScreen from "../../screens/admin/AdminDashboardScreen";
import AdminProposalScreen from "../../screens/admin/AdminProposalScreen";
import AdminTransactionScreen from "../../screens/admin/AdminTransactionScreen";
import { Provider } from "react-redux";
import store from "../../stores/StoreAdmin";

function AdminScreen(props) {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Route path="/admin/login" exact={true} component={AdminLoginScreen} />
        <Route path="/admin/register" component={AdminRegisterScreen} />
        <Route path="/admin/login" component={AdminLoginScreen} /> 
        <Route path="/admin/dashboard" component={AdminDashboardScreen} />
        <Route path="/admin/proposal" component={AdminProposalScreen} />
        <Route path="/admin/transaction" component={AdminTransactionScreen} />
      </BrowserRouter>
    </Provider>
  );
}
export default AdminScreen;
