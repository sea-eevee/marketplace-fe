import React from "react";
import BrandNavbar from "../../components/BrandNavbar";
import AdminHeader from "../../components/AdminHeader";
import AdminMenu from "../../components/AdminMenu";
import AdminDashboard from "../../components/AdminDashboard";
import { useSelector } from "react-redux";
import { Redirect } from "react-router-dom";

function AdminDashboardScreen(props) {
  const selector = (state) => {
    if (state.adminSignin.adminInfo) {
      return state.adminSignin;
    } else {
      return state.adminRegister;
    }
  };

  const adminSignin = useSelector((state) => selector(state));
  const { adminInfo } = adminSignin;

  return adminInfo ? (
    <div>
      <BrandNavbar />
      <AdminHeader />
      <AdminMenu />
      <AdminDashboard />
    </div>
  ) : (
    <Redirect
      to={{
        pathname: "/admin/login",
        state: {
          from: props.location,
        },
      }}
    />
  );
}

export default AdminDashboardScreen;
