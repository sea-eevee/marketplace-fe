import React, { useState } from "react";
import { Button, Card, Col, Container, Form } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { Redirect } from "react-router-dom";
import { merchantAddItem } from "../../actions/merchantActions";
import Footer from "../../components/Footer";
import MerchantNavbar from "../../components/merchant/MerchantNavbar";

function MerchantCreateItemScreen(props) {
  const dispatch = useDispatch();

  const [itemName, setItemName] = useState("");
  const [itemStock, setItemStock] = useState("");
  const [itemPrice, setItemPrice] = useState("");
  const [itemDescription, setItemDescription] = useState("");
  const [itemPicture, setItemPicture] = useState("");

  const merchantSignin = useSelector((state) => state.merchantSignin);
  const { merchantInfo } = merchantSignin;

  const submitHandler = async (e) => {
    e.preventDefault();
    if (merchantInfo) {
      await dispatch(
        merchantAddItem(merchantInfo.user_id, {
          stock: itemStock,
          display_name: itemName,
          price: itemPrice,
          description: itemDescription,
          picture: itemPicture,
        })
      );
      props.history.push("/merchant");
    }
  };

  return merchantInfo ? (
    <div className="grid-container">
      <header className="header bg-light">
        <MerchantNavbar merchantName={merchantInfo.username} />
      </header>
      <main className="main bg-light" style={{ minHeight: "80vh" }}>
        <Container className="my-5">
          <Card>
            <Card.Body>
              <Form onSubmit={submitHandler}>
                <Form.Group controlId="formGridItemName">
                  <Form.Label>Item Name</Form.Label>
                  <Form.Control onChange={(e) => setItemName(e.target.value)} />
                </Form.Group>

                <Form.Row>
                  <Form.Group as={Col} controlId="formGridStock">
                    <Form.Label>Stock</Form.Label>
                    <Form.Control
                      type="number"
                      onChange={(e) => setItemStock(e.target.value)}
                    />
                  </Form.Group>

                  <Form.Group as={Col} controlId="formGridPrice">
                    <Form.Label>Price</Form.Label>
                    <Form.Control
                      type="number"
                      onChange={(e) => setItemPrice(e.target.value)}
                    />
                  </Form.Group>
                </Form.Row>

                <Form.Group controlId="formGridDescription">
                  <Form.Label>Item Description</Form.Label>
                  <Form.Control
                    onChange={(e) => setItemDescription(e.target.value)}
                  />
                </Form.Group>

                <Form.Group controlId="formGridAddress2">
                  <Form.Label>Item picture</Form.Label>
                  <Form.Control
                    type="file"
                    onChange={(e) => setItemPicture(e.target.files[0])}
                  />
                </Form.Group>

                <Form.Group id="formGridCheckbox">
                  <Form.Check type="checkbox" label="Check me out" />
                </Form.Group>

                <Button variant="info" type="submit">
                  Submit
                </Button>
              </Form>
            </Card.Body>
          </Card>
        </Container>
      </main>
      <Footer />
    </div>
  ) : (
    <Redirect
      to={{
        pathname: "/merchant/login",
        state: {
          from: props.location,
        },
      }}
    />
  );
}
export default MerchantCreateItemScreen;
