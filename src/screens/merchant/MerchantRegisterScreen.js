import React, { useEffect, useState } from "react";
import BreadSeller from "../../images/bread-seller.svg";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { register } from "../../actions/merchantActions";
import BrandNavbar from "../../components/BrandNavbar";
import Footer from "../../components/Footer";
import { Card, Button, Container, Form } from "react-bootstrap";

function MerchantRegisterScreen(props) {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [rePassword, setRePassword] = useState("");
  const merchantRegister = useSelector((state) => state.merchantRegister);
  const { loading, merchantInfo, error } = merchantRegister;
  const dispatch = useDispatch();

  useEffect(() => {
    if (merchantInfo) {
      props.history.push("/merchant");
    }
    return () => {};
    // eslint-disable-next-line
  }, [merchantInfo]);

  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(register(name, email, password === rePassword && password));
  };

  return (
    <div className="grid-container">
      <header className="header">
      <BrandNavbar />
      </header>
      <main className="main">
      <Container
        fluid
        className="d-flex align-items-center justify-content-center py-5"
      >
        <div>
            <h1
            style={{textAlign: "center",color:"#17a2b8"}}>Be Humble Seller</h1>
            <img
            alt=""
            src={BreadSeller}
            width="500"
            height="500"
            className="d-none d-lg-block"
            />
        </div>
        <Card className="text-center">
          <Card.Header>Register</Card.Header>
          <Card.Body>
            <Form onSubmit={submitHandler}>
              <Form.Group
                controlId="formBasicName"
                className="d-flex flex-column align-items-start"
              >
                <Form.Label>Username</Form.Label>
                <Form.Control
                  placeholder="Enter unique username"
                  onChange={(e) => setName(e.target.value)}
                />
              </Form.Group>
              <Form.Group
                controlId="formBasicEmail"
                className="d-flex flex-column align-items-start"
              >
                <Form.Label>Email Address</Form.Label>
                <Form.Control
                  type="email"
                  placeholder="Enter email"
                  onChange={(e) => setEmail(e.target.value)}
                />
                <Form.Text className="text-muted">
                  We'll never share your email with anyone else.
                </Form.Text>
              </Form.Group>
              <Form.Group
                controlId="formBasicPassword"
                className="d-flex flex-column align-items-start"
              >
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Password"
                  onChange={(e) => setPassword(e.target.value)}
                />
              </Form.Group>
              <Form.Group
                controlId="formBasicRePassword"
                className="d-flex flex-column align-items-start"
              >
                <Form.Label>Re-Enter Password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Confirm Password"
                  onChange={(e) => setRePassword(e.target.value)}
                />
              </Form.Group>
              <Button variant="info" type="submit">
                Submit
              </Button>
            </Form>
            {loading && <Card.Text>Loading..</Card.Text>}
            {error && <Card.Text>{error}</Card.Text>}
          </Card.Body>
          <Card.Footer>
            <p>Already Become a Seller?</p>
            <Link to="/merchant/login">Sign In</Link>
          </Card.Footer>
        </Card>
      </Container>
      </main>
      <Footer />
    </div>
  );
}

export default MerchantRegisterScreen;