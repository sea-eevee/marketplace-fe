import React, { useState } from "react";
import Footer from "../../components/Footer";
import { Container, Card } from "react-bootstrap";
import MerchantSideContainer from "../../components/merchant/MerchantSideContainer";
import MerchantProfileTab from "../../components/merchant/MerchantProfileTab";
import MerchantTransactionItem from "../../components/merchant/MerchantTransactionItem";
import MerchantNavbar from "../../components/merchant/MerchantNavbar";
import { useSelector } from "react-redux";
import { Redirect } from "react-router-dom";

function MerchantTransactionScreen(props) {
  const merchantSignin = useSelector((state) => state.merchantSignin);
  const { merchantInfo } = merchantSignin;

  const merchant = {
  };

  return merchantInfo ? (
    <div className="grid-container">
      <header className="header bg-light">
        <MerchantNavbar merchantName={merchantInfo.username} />
      </header>
      <main className="main bg-light">
        <Container className="d-flex py-5">
          <MerchantSideContainer data={merchant} navActiveKey="link-1" />
          <div className="flex-fill d-flex flex-column">
            <MerchantProfileTab defaultActiveKey="link-2" />
            <Card>
              <Card.Body>
              </Card.Body>
            </Card>
          </div>
        </Container>
      </main>
      <Footer />
    </div>
  ) : (
    <Redirect
      to={{
        pathname: "/merchant/login",
        state: {
          from: props.location,
        },
      }}
    />
  );
}
export default MerchantTransactionScreen;