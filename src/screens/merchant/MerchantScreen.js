import React from "react";
import { BrowserRouter, Route } from "react-router-dom";

import MerchantRegisterScreen from "../merchant/MerchantRegisterScreen";
import MerchantSigninScreen from "../merchant/MerchantLoginScreen";
import MerchantProfileScreen from "./MerchantProfileScreen";
import { Provider } from "react-redux";
import store from "../../stores/StoreMerchant";
import MerchantTransactionScreen from "./MerchantTransactionScreen";
import MerchantHomeScreen from "./MerchantHomeScreen";
import MerchantCreateItemScreen from "./MerchantCreateItemScreen";

function MerchantScreen(props) {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Route path="/merchant" exact={true} component={MerchantHomeScreen} />
        <Route
          path="/merchant/profile"
          exact={true}
          component={MerchantProfileScreen}
        />
        <Route path="/merchant/register" component={MerchantRegisterScreen} />
        <Route path="/merchant/login" component={MerchantSigninScreen} />
        <Route
          path="/merchant/history"
          exact={true}
          component={MerchantTransactionScreen}
        />
        <Route
          path="/merchant/createitem"
          component={MerchantCreateItemScreen}
        />
      </BrowserRouter>
    </Provider>
  );
}

export default MerchantScreen;
