import React from "react";
import { useSelector } from "react-redux";
import { Container, Card, Form, Button, Col } from "react-bootstrap";
import Footer from "../../components/Footer";
import MerchantSideContainer from "../../components/merchant/MerchantSideContainer";
import MerchantProfileTab from "../../components/merchant/MerchantProfileTab";
import MerchantNavbar from "../../components/merchant/MerchantNavbar";
import { Redirect } from "react-router-dom";

function MerchantHomeScreen(props) {
  const merchantSignin = useSelector((state) => state.merchantSignin);
  const { merchantInfo } = merchantSignin;
  console.log(merchantInfo);

  const merchant = {
  };

  return merchantInfo ? (
    <div className="grid-container">
      <header className="header bg-light">
        <MerchantNavbar
          merchantName={merchantInfo.username}
        />
      </header>
      <main className="main bg-light">
        <Container className="d-flex py-5">
          <MerchantSideContainer data={merchant} navActiveKey="link-1" />
          <div className="flex-fill d-flex flex-column">
            <MerchantProfileTab defaultActiveKey="link-1" />
            <Card>
              <Card.Body>
                <Form>
                  <Form.Group controlId="formGridName">
                    <Form.Label>Name</Form.Label>
                    <Form.Control placeholder="Your Name" />
                  </Form.Group>

                  <Form.Group controlId="formGridAddress">
                    <Form.Label>Address</Form.Label>
                    <Form.Control placeholder="Gedung Blok Z, Jl. Angrek no. 3, Rt04 / Rw09" />
                  </Form.Group>

                  <Form.Row>
                    <Form.Group as={Col} controlId="formGridCity">
                      <Form.Label>City</Form.Label>
                      <Form.Control />
                    </Form.Group>

                    <Form.Group as={Col} controlId="formGridProvince">
                      <Form.Label>Province</Form.Label>
                      <Form.Control />
                    </Form.Group>
                  </Form.Row>

                  <Form.Group id="formGridCheckbox">
                    <Form.Check type="checkbox" label="Check me out" />
                  </Form.Group>

                  <Button variant="info" type="submit">
                    Update
                  </Button>
                </Form>
              </Card.Body>
            </Card>
          </div>
        </Container>
      </main>
      <Footer />
    </div>
  ) : (
    <Redirect
      to={{
        pathname: "/merchant/login",
        state: {
          from: props.location,
        },
      }}
    />
  );
}

export default MerchantHomeScreen;
