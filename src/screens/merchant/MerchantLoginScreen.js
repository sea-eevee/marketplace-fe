import React, { useEffect, useState } from "react";
import BreadSeller from "../../images/bread-seller.svg";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { signin } from "../../actions/merchantActions";
import BrandNavbar from "../../components/BrandNavbar";
import Footer from "../../components/Footer";
import { Card, Button, Container, Form } from "react-bootstrap";

function MerchantSigninScreen(props) {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const merchantSignin = useSelector((state) => state.merchantSignin);
  const { loading, merchantInfo, error } = merchantSignin;
  const dispatch = useDispatch();
  
  useEffect(() => {
    if (merchantInfo) {
      props.history.push("/merchant");
    }
    return () => {};
    // eslint-disable-next-line
  }, [merchantInfo]);

  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(signin(username, password));
  };

  return (
    <div className="grid-container">
      <header className="header">
        <BrandNavbar />
      </header>
      <main className="main">
        <Container
          fluid
          className="d-flex align-items-center justify-content-center py-5"
        >
          <img
            alt=""
            src={BreadSeller}
            width="400"
            height="400"
            className="d-none d-lg-block"
          />
          <Card className="text-center">
            <Card.Header>Login</Card.Header>
            <Card.Body>
              <Form onSubmit={submitHandler}>
                <Form.Group
                  controlId="formBasicEmail"
                  className="d-flex flex-column align-items-start"
                >
                  <Form.Label>Username</Form.Label>
                  <Form.Control
                    placeholder="Enter your username"
                    onChange={(e) => setUsername(e.target.value)}
                  />
                </Form.Group>
                <Form.Group
                  controlId="formBasicPassword"
                  className="d-flex flex-column align-items-start"
                >
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    type="password"
                    placeholder="Password"
                    onChange={(e) => setPassword(e.target.value)}
                  />
                </Form.Group>
                <Button variant="info" type="submit">
                  Submit
                </Button>
              </Form>
              {loading && <Card.Text>Loading..</Card.Text>}
              {error && <Card.Text>{error}</Card.Text>}
            </Card.Body>
            <Card.Footer>
              <p>New to this?</p>
              <Link
                to="/merchant/register"
                className="button secondary text-center"
              >
                Become a seller
              </Link>
            </Card.Footer>
          </Card>
        </Container>
      </main>
      <Footer />
    </div>
  );
}

export default MerchantSigninScreen;
