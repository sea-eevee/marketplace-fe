import React, { useEffect, useState } from "react";
import { Button, Container } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { Redirect } from "react-router-dom";
import { merchantListItem } from "../../actions/merchantActions";
import Footer from "../../components/Footer";
import MerchantCrudItem from "../../components/merchant/MerchantCrudItem";
import MerchantNavbar from "../../components/merchant/MerchantNavbar";
import MerchantSideContainer from "../../components/merchant/MerchantSideContainer";

function MerchantHomeScreen(props) {
  const dispatch = useDispatch();
  const selector = (state) => {
    if (state.merchantSignin.merchantInfo) {
      return state.merchantSignin;
    } else {
      return state.merchantRegister;
    }
  };
  const merchantSignin = useSelector((state) => selector(state));
  const { merchantInfo } = merchantSignin;

  const merchantlistItem = useSelector((state) => state.merchantListItem);
  const { items, loading, error } = merchantlistItem;
  console.log(merchantlistItem);

  useEffect(() => {
    if (merchantInfo) {
      dispatch(merchantListItem(merchantInfo.user_id));
    }
    // eslint-disable-next-line
  }, []);
  const merchant = {};

  return merchantInfo ? (
    <div className="grid-container">
      <header className="header bg-light">
        <MerchantNavbar merchantName={merchantInfo.username} />
      </header>
      <main className="main bg-light">
        <Container className="d-flex py-5">
          <MerchantSideContainer data={merchant} navActiveKey="link-3" />
          <div className="flex-fill d-flex flex-column">
            <Button
              variant="success"
              href="/merchant/createitem"
              className="ml-auto"
              style={{ width: "40%" }}
            >
              Create New Item
            </Button>
            {loading ? (
              <div>Loading..</div>
            ) : error ? (
              <div>{error}</div>
            ) : (
              items.map((item) => (
                <MerchantCrudItem item={item} key={item.ItemID} />
              ))
            )}
          </div>
        </Container>
      </main>
      <Footer />
    </div>
  ) : (
    <Redirect
      to={{
        pathname: "/merchant/login",
        state: {
          from: props.location,
        },
      }}
    />
  );
}
export default MerchantHomeScreen;
