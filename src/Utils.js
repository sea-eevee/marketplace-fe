function LimitCharacters(num, str) {
  if (str.length > num) {
    let res = str.substr(0, num);
    return `${res}...`;
  }
  return str;
}


export {LimitCharacters};