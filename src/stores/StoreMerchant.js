import {createStore, combineReducers, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import Cookie from 'js-cookie';
import { merchantAddItemReducer, merchantListItemReducer, merchantRegisterReducer, merchantSigninReducer } from '../reducers/merchantReducers';

const merchantInfo = Cookie.getJSON("merchantInfo") || null;


const initialState = { merchantSignin: { merchantInfo } };
const reducer = combineReducers({ 
    merchantSignin: merchantSigninReducer,
    merchantRegister: merchantRegisterReducer,
    merchantListItem: merchantListItemReducer,
    merchantAddItem: merchantAddItemReducer
})

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducer, initialState, composeEnhancer(applyMiddleware(thunk)));
export default store;
