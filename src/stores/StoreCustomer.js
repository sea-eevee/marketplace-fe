import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import Cookie from "js-cookie";
import {
  userSigninReducer,
  userRegisterReducer,
  userListMerchantReducer,
  userDetailMerchantReducer,
  userProfileReducer,
  userReadCartReducer,
  userAddCartReducer,
  userProfileUpdateReducer, editItemCartReducer, userReadTransactionReducer, userListOrderReducer, userPaidOrderReducer
} from "../reducers/customerReducers";

const userInfo = Cookie.getJSON("userInfo") || null;

const initialState = {
  userSignin: { userInfo },
  userListMerchant: { merchants: [] },
};
const reducer = combineReducers({
  userSignin: userSigninReducer,
  userRegister: userRegisterReducer,
  userProfileUpdate: userProfileUpdateReducer,
  userReadCart: userReadCartReducer,
  userAddCart: userAddCartReducer,
  userProfile: userProfileReducer,
  userListMerchant: userListMerchantReducer,
  userDetailMerchant: userDetailMerchantReducer,
  editItemCart: editItemCartReducer,
  userTransaction: userReadTransactionReducer,
  userListOrder: userListOrderReducer,
  userPaidOrder: userPaidOrderReducer
});

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  reducer,
  initialState,
  composeEnhancer(applyMiddleware(thunk))
);
export default store;
