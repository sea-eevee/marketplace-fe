import {createStore, combineReducers, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import Cookie from 'js-cookie';
import { 
    adminSigninReducer, 
    adminRegisterReducer, 
    adminDashboardReducer, 
    adminProposalReducer,
    adminAcceptProposalReducer,
    adminRejectProposalReducer,
    adminTransactionReducer,
    adminAcceptTransactionReducer,
    adminRejectTransactionReducer
 } from '../reducers/adminReducers';

const adminInfo = Cookie.getJSON("adminInfo") || null;

const initialState = { adminSignin: { adminInfo} };
const reducer = combineReducers({ 
    adminSignin: adminSigninReducer,
    adminRegister: adminRegisterReducer,
    adminDashboard: adminDashboardReducer,
    adminProposal: adminProposalReducer,
    adminAcceptProposal: adminAcceptProposalReducer,
    adminRejectProposal: adminRejectProposalReducer,
    adminTransaction: adminTransactionReducer,
    adminAcceptTransaction: adminAcceptTransactionReducer,
    adminRejectTransaction: adminRejectTransactionReducer
})

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducer, initialState, composeEnhancer(applyMiddleware(thunk)));
export default store;