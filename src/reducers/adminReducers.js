const { 
    ADMIN_SIGNIN_REQUEST, 
    ADMIN_SIGNIN_SUCCESS, 
    ADMIN_SIGNIN_FAIL, 
    ADMIN_REGISTER_REQUEST, 
    ADMIN_REGISTER_SUCCESS,
    ADMIN_REGISTER_FAIL, 
    ADMIN_DASHBOARD_REQUEST, 
    ADMIN_DASHBOARD_SUCCESS,
    ADMIN_DASHBOARD_FAIL, 
    ADMIN_PROPOSAL_REQUEST,
    ADMIN_PROPOSAL_SUCCESS,
    ADMIN_PROPOSAL_FAIL,
    ADMIN_ACCEPTPROPOSAL_REQUEST,
    ADMIN_ACCEPTPROPOSAL_SUCCESS,
    ADMIN_ACCEPTPROPOSAL_FAIL,
    ADMIN_REJECTPROPOSAL_REQUEST,
    ADMIN_REJECTPROPOSAL_SUCCESS,
    ADMIN_REJECTPROPOSAL_FAIL,
    ADMIN_TRANSACTION_REQUEST,
    ADMIN_TRANSACTION_SUCCESS,
    ADMIN_TRANSACTION_FAIL,
    ADMIN_ACCEPTTRANSACTION_REQUEST,
    ADMIN_ACCEPTTRANSACTION_SUCCESS,
    ADMIN_ACCEPTTRANSACTION_FAIL,
    ADMIN_REJECTTRANSACTION_REQUEST,
    ADMIN_REJECTTRANSACTION_SUCCESS,
    ADMIN_REJECTTRANSACTION_FAIL
} = require("../constants/adminConstants");

function adminSigninReducer(state={data:{username:''}}, action){
    switch(action.type){
        case ADMIN_SIGNIN_REQUEST:
            return {loading: true, data:{username:''}};
        case ADMIN_SIGNIN_SUCCESS:
            return {loading: false, adminInfo: action.payload};
        case ADMIN_SIGNIN_FAIL:
            return {loading: false, error: action.payload};
        default:
            return state;
    }
}

function adminRegisterReducer(state={data:{username:''}}, action){
    switch(action.type){
        case ADMIN_REGISTER_REQUEST:
            return {loading: true, data:{username:''}};
        case ADMIN_REGISTER_SUCCESS:
            return {loading: false, adminInfo: action.payload};
        case ADMIN_REGISTER_FAIL:
            return {loading: false, error: action.payload};
        default:
            return state;
    }
}

function adminDashboardReducer(state={data:{count_new_costumer:'', count_new_merchant:'', count_new_transaction:'', count_product:''}}, action){
    switch(action.type){
        case ADMIN_DASHBOARD_REQUEST:
            return {loading: true, data:{count_new_costumer:'', count_new_merchant:'', count_new_transaction:'', count_product:''}};
        case ADMIN_DASHBOARD_SUCCESS:
            return {loading: false, data: action.payload};
        case ADMIN_DASHBOARD_FAIL:
            return {loading: false, error: action.payload};
        default:
            return state;
    }
}

function adminProposalReducer(state={data:{merchant_proposal:[]}}, action){
    switch(action.type){
        case ADMIN_PROPOSAL_REQUEST:
            return {loading: true, data:{merchant_proposal:[]}};
        case ADMIN_PROPOSAL_SUCCESS:
            return {loading: false, data: action.payload};
        case ADMIN_PROPOSAL_FAIL:
            return {loading: false, error: action.payload};
        default:
            return state;
    }
}

function adminAcceptProposalReducer(state={}, action){
    switch(action.type){
        case ADMIN_ACCEPTPROPOSAL_REQUEST:
            return {loading: true};
        case ADMIN_ACCEPTPROPOSAL_SUCCESS:
            return {loading: false, adminInfo: action.payload};
        case ADMIN_ACCEPTPROPOSAL_FAIL:
            return {loading: false, error: action.payload};
        default:
            return state;
    }
}

function adminRejectProposalReducer(state={}, action){
    switch(action.type){
        case ADMIN_REJECTPROPOSAL_REQUEST:
            return {loading: true};
        case ADMIN_REJECTPROPOSAL_SUCCESS:
            return {loading: false, adminInfo: action.payload};
        case ADMIN_REJECTPROPOSAL_FAIL:
            return {loading: false, error: action.payload};
        default:
            return state;
    }
}

function adminTransactionReducer(state={data:{transactions:[]}}, action){
    switch(action.type){
        case ADMIN_TRANSACTION_REQUEST:
            return {loading: true, data:{transactions:[]}};
        case ADMIN_TRANSACTION_SUCCESS:
            return {loading: false, data: action.payload};
        case ADMIN_TRANSACTION_FAIL:
            return {loading: false, error: action.payload};
        default:
            return state;
    }
}

function adminAcceptTransactionReducer(state={}, action){
    switch(action.type){
        case ADMIN_ACCEPTTRANSACTION_REQUEST:
            return {loading: true};
        case ADMIN_ACCEPTTRANSACTION_SUCCESS:
            return {loading: false, adminInfo: action.payload};
        case ADMIN_ACCEPTTRANSACTION_FAIL:
            return {loading: false, error: action.payload};
        default:
            return state;
    }
}

function adminRejectTransactionReducer(state={}, action){
    switch(action.type){
        case ADMIN_REJECTTRANSACTION_REQUEST:
            return {loading: true};
        case ADMIN_REJECTTRANSACTION_SUCCESS:
            return {loading: false, adminInfo: action.payload};
        case ADMIN_REJECTTRANSACTION_FAIL:
            return {loading: false, error: action.payload};
        default:
            return state;
    }
}

export { adminSigninReducer, adminRegisterReducer, adminDashboardReducer, adminProposalReducer, adminAcceptProposalReducer, adminRejectProposalReducer, adminTransactionReducer, adminAcceptTransactionReducer, adminRejectTransactionReducer }