const {
  MERCHANT_SIGNIN_REQUEST,
  MERCHANT_SIGNIN_SUCCESS,
  MERCHANT_SIGNIN_FAIL,
  MERCHANT_REGISTER_REQUEST,
  MERCHANT_REGISTER_SUCCESS,
  MERCHANT_REGISTER_FAIL,
  MERCHANT_LIST_ITEM_REQUEST,
  MERCHANT_LIST_ITEM_SUCCESS,
  MERCHANT_LIST_ITEM_FAIL,
  MERCHANT_ADD_ITEM_REQUEST,
  MERCHANT_ADD_ITEM_SUCCESS,
  MERCHANT_ADD_ITEM_FAIL
} = require("../constants/merchantConstants");

function merchantSigninReducer(state = {}, action) {
  switch (action.type) {
    case MERCHANT_SIGNIN_REQUEST:
      return { loading: true };
    case MERCHANT_SIGNIN_SUCCESS:
      return { loading: false, merchantInfo: action.payload };
    case MERCHANT_SIGNIN_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

function merchantRegisterReducer(state = {}, action) {
  switch (action.type) {
    case MERCHANT_REGISTER_REQUEST:
      return { loading: true };
    case MERCHANT_REGISTER_SUCCESS:
      return { loading: false, merchantInfo: action.payload };
    case MERCHANT_REGISTER_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

function merchantListItemReducer(state = {items: []}, action) {
  switch (action.type) {
    case MERCHANT_LIST_ITEM_REQUEST:
      return { loading: true, items: [] };
    case MERCHANT_LIST_ITEM_SUCCESS:
      return { loading: false, items: action.payload };
    case MERCHANT_LIST_ITEM_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

function merchantAddItemReducer(state = {status: ""}, action) {
  switch (action.type) {
    case MERCHANT_ADD_ITEM_REQUEST:
      return { loading: true, status: "" };
    case MERCHANT_ADD_ITEM_SUCCESS:
      return { loading: false, status: action.payload };
    case MERCHANT_ADD_ITEM_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

export { merchantSigninReducer, merchantRegisterReducer, merchantListItemReducer, merchantAddItemReducer };
