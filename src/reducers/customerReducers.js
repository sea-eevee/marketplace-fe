const {
  USER_SIGNIN_REQUEST,
  USER_SIGNIN_SUCCESS,
  USER_SIGNIN_FAIL,
  USER_REGISTER_REQUEST,
  USER_REGISTER_SUCCESS,
  USER_REGISTER_FAIL,
  USER_LIST_MERCHANT_REQUEST,
  USER_LIST_MERCHANT_SUCCESS,
  USER_LIST_MERCHANT_FAIL,
  USER_DETAIL_MERCHANT_REQUEST,
  USER_DETAIL_MERCHANT_SUCCESS,
  USER_DETAIL_MERCHANT_FAIL,
  USER_PROFILE_REQUEST,
  USER_PROFILE_SUCCESS,
  USER_PROFILE_FAIL,
  USER_READ_CART_REQUEST,
  USER_READ_CART_SUCCESS,
  USER_READ_CART_FAIL,
  USER_ADD_CART_REQUEST,
  USER_ADD_CART_SUCCESS,
  USER_ADD_CART_FAIL,
  USER_PROFILE_UPDATE_REQUEST,
  USER_PROFILE_UPDATE_SUCCESS,
  USER_PROFILE_UPDATE_FAIL,
  CART_QUANTITY_EDIT_SUCCESS,
  CART_QUANTITY_EDIT_REQUEST,
  CART_QUANTITY_EDIT_FAIL,
  USER_READ_TRANSACTION_REQUEST,
  USER_READ_TRANSACTION_SUCCESS,
  USER_READ_TRANSACTION_FAIL,
  USER_LIST_ORDER_REQUEST,
  USER_LIST_ORDER_SUCCESS,
  USER_LIST_ORDER_FAIL,
  USER_PAID_ORDER_REQUEST,
  USER_PAID_ORDER_SUCCESS,
  USER_PAID_ORDER_FAIL,
  USER_CART_ORDER_REQUEST,
  USER_CART_ORDER_SUCCESS,
  USER_CART_ORDER_FAIL

} = require("../constants/customerConstants");

function userSigninReducer(state = {}, action) {
  switch (action.type) {
    case USER_SIGNIN_REQUEST:
      return { loading: true };
    case USER_SIGNIN_SUCCESS:
      return { loading: false, userInfo: action.payload };
    case USER_SIGNIN_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

function userRegisterReducer(state = {}, action) {
  switch (action.type) {
    case USER_REGISTER_REQUEST:
      return { loading: true };
    case USER_REGISTER_SUCCESS:
      return { loading: false, userInfo: action.payload };
    case USER_REGISTER_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

function userProfileReducer(state = {customer_profile:{}}, action) {
  switch (action.type) {
    case USER_PROFILE_REQUEST:
      return { loading: true, customer_profile:{} };
    case USER_PROFILE_SUCCESS:
      return { loading: false, customer_profile: action.payload.customer_profile };
    case USER_PROFILE_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

function userProfileUpdateReducer(state = {customer_profile:{}}, action) {
  switch (action.type) {
    case USER_PROFILE_UPDATE_REQUEST:
      return { loading: true, customer_profile:{} };
    case USER_PROFILE_UPDATE_SUCCESS:
      return { loading: false, customer_profile: action.payload.customer_profile };
    case USER_PROFILE_UPDATE_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

function userReadCartReducer(state = { items: [] }, action) {
  switch (action.type) {
    case USER_READ_CART_REQUEST:
      return { loading: true, items: []  };
    case USER_READ_CART_SUCCESS:
      return { loading: false, items: action.payload };
    case USER_READ_CART_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

function userAddCartReducer(state = { data: { } }, action) {
  switch (action.type) {
    case USER_ADD_CART_REQUEST:
      return { loading: true, data: { } };
    case USER_ADD_CART_SUCCESS:
      return { loading: false, data: action.payload };
    case USER_ADD_CART_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

function editItemCartReducer(state = { data: "" }, action) {
  switch (action.type) {
    case CART_QUANTITY_EDIT_REQUEST:
      return { loading: true, data: "" };
    case CART_QUANTITY_EDIT_SUCCESS:
      return { loading: false, data: action.payload };
    case CART_QUANTITY_EDIT_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

function userReadTransactionReducer(state = { transactions: [] }, action) {
  switch (action.type) {
    case USER_READ_TRANSACTION_REQUEST:
      return { loading: true, transactions: []  };
    case USER_READ_TRANSACTION_SUCCESS:
      return { loading: false, transactions: action.payload };
    case USER_READ_TRANSACTION_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

function userListMerchantReducer(state = {}, action) {
  switch (action.type) {
    case USER_LIST_MERCHANT_REQUEST:
      return { loading: true };
    case USER_LIST_MERCHANT_SUCCESS:
      return { loading: false, merchants: action.payload.merchants };
    case USER_LIST_MERCHANT_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

function userDetailMerchantReducer(state = { data: { merchant: {location:{}}, items: [] } }, action) {
  switch (action.type) {
    case USER_DETAIL_MERCHANT_REQUEST:
      return { loading: true, data: { merchant: {location:{}}, items: [] } };
    case USER_DETAIL_MERCHANT_SUCCESS:
      return { loading: false, data: action.payload };
    case USER_DETAIL_MERCHANT_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

function userListOrderReducer(state = {customer_order: []}, action) {
  switch (action.type) {
    case USER_LIST_ORDER_REQUEST:
      return { loading: true, customer_order: [] };
    case USER_LIST_ORDER_SUCCESS:
      return { loading: false, customer_order: action.payload };
    case USER_LIST_ORDER_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

function userPaidOrderReducer(state = {status: ""}, action) {
  switch (action.type) {
    case USER_PAID_ORDER_REQUEST:
      return { loading: true, status: "" };
    case USER_PAID_ORDER_SUCCESS:
      return { loading: false, status: action.payload };
    case USER_PAID_ORDER_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

function userCartOrderReducer(state = {status: ""}, action) {
  switch (action.type) {
    case USER_CART_ORDER_REQUEST:
      return { loading: true, status: "" };
    case USER_CART_ORDER_SUCCESS:
      return { loading: false, status: action.payload };
    case USER_CART_ORDER_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
}

export {
  userSigninReducer,
  userRegisterReducer,
  userProfileReducer,
  userProfileUpdateReducer,
  userListMerchantReducer,
  userDetailMerchantReducer,
  userReadCartReducer,
  userAddCartReducer,
  editItemCartReducer,
  userReadTransactionReducer,
  userListOrderReducer,
  userPaidOrderReducer,
  userCartOrderReducer
};
