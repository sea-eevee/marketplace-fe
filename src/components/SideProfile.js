import React from "react";
import { Card } from "react-bootstrap";
import BlankAvatar from "../blank-avatar.png";

function SideProfile(props) {
  return (
    <Card style={{ width: "18rem", height: "30rem", margin: "2rem" }}>
      <Card.Body>
        <div className="d-flex flex-column mb-3">
          <div className="d-flex mb-3">
            <img
              alt=""
              src={BlankAvatar}
              width="60"
              height="60"
              className="mx-3"
            />
            <h6 className="mt-3">{props.data.display_name}</h6>
          </div>
          <div className="border-bottom border-secondary mb-3"></div>
          <div className="text-secondary font-weight-bolder">address</div>
          <div className="mb-3">
            {props.data.address}
          </div>
        </div>
        {props.children}
      </Card.Body>
    </Card>
  );
}
export default SideProfile;
