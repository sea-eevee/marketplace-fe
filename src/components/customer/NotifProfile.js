import React from "react";
import { Button, Card } from "react-bootstrap";
import { Link } from "react-router-dom";

function NotifProfile(props) {
  return (
    <Card bg="warning" className="mb-5">
      <Link
        to={`/customer/profile`}
        style={{ color: "inherit", textDecoration: "inherit" }}
      >
        <Card.Body>
          <div className="d-flex justify-content-between px-5">
            <Card.Title>Please Update your profile</Card.Title>
            <Button
              href="/customer/profile"
              variant="info"
              className="font-weight-bolder"
            >
              Update Profile
            </Button>
          </div>
        </Card.Body>
      </Link>
    </Card>
  );
}
export default NotifProfile;
