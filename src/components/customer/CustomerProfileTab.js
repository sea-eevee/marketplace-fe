import React from "react";
import { Nav } from "react-bootstrap";

function CustomerProfileTab(props) {
  return (
    <Nav fill justify variant="tabs" defaultActiveKey={props.defaultActiveKey}>
      <Nav.Item>
        <Nav.Link href="/customer/profile" eventKey="link-1">
          Edit Profile
        </Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link href="/customer/history" eventKey="link-2">
          Transaction History
        </Nav.Link>
      </Nav.Item>
    </Nav>
  );
}
export default CustomerProfileTab;
