import React from "react";
import Cookie from "js-cookie";
import { Navbar, Nav, NavDropdown } from "react-bootstrap";
import Cart from "../../images/cart.svg";
import { Link } from "react-router-dom";

function CustomerNavbar(props) {
  const logOutClick = () => {
    Cookie.remove("userInfo");
    window.location.reload();
  };

  return (
    <Navbar fixed="top" bg="dark" variant="dark" expand="md">
      <Navbar.Brand href="/">Eeeve Marketplace</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link href="/">Home</Nav.Link>
        </Nav>
        <Nav className="d-flex align-items-center mr-5 px-5">
          <Link to="/customer/cart" className="mr-3">
            <img
              alt=""
              src={Cart}
              width="30"
              height="30"
              className="d-inline-block align-top"
            />
          </Link>
          <NavDropdown title={props.customerName} id="basic-nav-dropdown">
            <NavDropdown.Item href="/customer/profile">
              Profile
            </NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item href="#" onClick={logOutClick}>
              Log Out
            </NavDropdown.Item>
          </NavDropdown>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}
export default CustomerNavbar;
