import React from "react";
import { Card } from "react-bootstrap";

function CustomerTransactionItem(props) {
  const ts = new Date(props.item.transaction_date);
  return (
    <Card border="secondary" className="my-3">
      <Card.Body>
        <Card.Link
          style={{ textDecoration: "none", color: "black" }}
        >
          <div>
            <div className="font-weight-bolder">
              Transaction ID: {props.item.transaction_id}
            </div>
            <div className="font-weight-light">{ts.toTimeString()}</div>
          </div>
        </Card.Link>
        <div className="border-bottom border-secondary mb-3"></div>
        <div className="d-flex">
          <Card.Link
            className="flex-fill"
            style={{ textDecoration: "none", color: "black" }}
          >
            <div className="ml-3 mb-5">
              <div
                className="font-weight-bolder"
                style={{ fontSize: "1.2rem" }}
              >
                {props.item.name}
              </div>
              <div>{props.item.bank_name}</div>
              <div className="d-flex">
                <div className="mr-1 font-weight-light">Bank Number:</div>
                <div className="text-success font-weight-bolder">
                  {props.item.bank_account_number}
                </div>
              </div>
            </div>
          </Card.Link>
        </div>
      </Card.Body>
    </Card>
  );
}

export default CustomerTransactionItem;
