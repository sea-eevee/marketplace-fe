import React from "react";
import Logo from "../images/logo.svg";
import { Navbar } from "react-bootstrap";

function BrandNavbar(props) {
  return (
    <Navbar bg="light">
      <Navbar.Brand href="/" className="mx-auto">
        <img
          alt=""
          src={Logo}
          width="30"
          height="30"
          className="d-inline-block align-top"
        />{" "}
        Eevee Marketplace
      </Navbar.Brand>
    </Navbar>
  );
}

export default BrandNavbar;
