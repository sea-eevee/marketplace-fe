import React from "react";
import { Navbar } from "react-bootstrap";

function Footer(props) {
  return <footer className="footer bg-dark text-white">
    <Navbar sticky="bottom"  bg="dark" variant="dark" >All right reserved.</Navbar>
    </footer>;
}
export default Footer;