import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getAdminTransaction, acceptAdminTransaction, rejectAdminTransaction } from "../actions/adminActions";

export default function AdminProposal() {
  const dispatch = useDispatch();
  //somethingChanged is raised when you click confirmation buttons
  const [somethingChanged, raiseSomethingChanged] = useState(false);
  const selector = (state) => {
    if (state.adminSignin.adminInfo) {
      return state.adminSignin;
    } else {
      return state.adminRegister;
    }
  };

  const adminSignin = useSelector((state) => selector(state));
  const { adminInfo } = adminSignin;
  
  //apiData is the response you get from:
  //curl --location --request GET "http://3.89.42.7/api/authed/admin/transaction" --header 'X-UserID: 3'
  /*
  const apiData = {
    "status": 0,
    "description": "success",
    "data": {
        "transactions": [{
            "transaction_id": 1,
            "transaction_date": 1599710985152,
            "customer": {
                "customer_id": 1,
                "display_name": "Lorem ipsum",
                "address": "Lorem ipsum"
            },
            "merchant": {
                "merchant_id": 1,
                "display_name": "Toko John Doe",
                "location_id": 1,
                "description": "Lorem Ipsum",
                "photo_url": "https://gitlab.com/sea-eevee/"
            },
            "items": [{
                "item_id": 1,
                "merchant_id": 0,
                "display_name": "Dadu",
                "price": 3000,
                "description": "Ini adalah sebuah dadu",
                "image": "https://sc01.alicdn.com/kf/H0a4dd4b62de04493a459ea4e900c8edfB.jpg_350x350.jpg"
            }, {
                "item_id": 2,
                "merchant_id": 0,
                "display_name": "Pizza",
                "price": 50000,
                "description": "Ini adalah sebuah pizza",
                "image": "https://edscyclopedia.com/wp-content/uploads/2016/07/dadu_merah-300x296.png"
            }, {
                "item_id": 3,
                "merchant_id": 0,
                "display_name": "Meja",
                "price": 200000,
                "description": "Ini adalah sebuah meja",
                "image": "https://cdn.monotaro.id/media/catalog/product/cache/6/image/b5fa40980320eb406ba395dece54e4a8/S/0/S004629955-1.jpg"
            }, {
                "item_id": 4,
                "merchant_id": 0,
                "display_name": "Kursi",
                "price": 3000,
                "description": "Ini adalah sebuah kursi",
                "image": "https://static.bmdstatic.com/pk/product/medium/5bc7e4e6674f3.jpg"
            }]
        }]
    }
};
*/
  const adminTransaction = useSelector((state) => state.adminTransaction);
  const { data, loading, error } = adminTransaction;

  useEffect(() => {
    dispatch(getAdminTransaction());
  }, [somethingChanged]);

  const renderHeader = () => {
    let headerElement = [
      "TransactionID",
      "MerchantID",
      "Merchant",
      "CustomerID",
      "Customer",
      "ProductID",
      "Product",
      "Price",
      "Confirmation",
    ];

    return headerElement.map((key, index) => {
      return <th key={index}>{key}</th>;
    });
  };

  const renderBody = () => {
    return (
      data &&
      data.transactions.map(({transaction_id, customer, merchant, items }) => {
        return (
          <tr key={transaction_id}>
            <td>{transaction_id}</td>
            <td>{merchant.merchant_id}</td>
            <td>{merchant.display_name}</td>
            <td>{customer.customer_id}</td>
            <td>{customer.display_name}</td>
            <td>
              {items.map(({item_id}) => {
                  return (
                      <tr>
                        <td>{item_id}</td>
                      </tr>
                  )
              })}
            </td>
            
            <td>
              {items.map(({display_name}) => {
                  return (
                      <tr>
                        <td>{display_name}</td>
                      </tr>
                  )
              })}
            </td>
            <td>
              {items.map(({price}) => {
                  return (
                      <tr>
                        <td>{price}</td>
                      </tr>
                  )
              })}
            </td>
            <td>
              <button
                className="btn"
                style={{
                  backgroundColor: "lightgreen",
                  borderRadius: "3px",
                  border: "none",
                  color: "white",
                  marginLeft: "2px",
                  marginRight: "18px",
                }}
                onClick={() => {dispatch(acceptAdminTransaction(transaction_id)); raiseSomethingChanged(true)}}
              >
                <i className="fas fa-check" />
              </button>

              <button
                className="btn"
                style={{
                  backgroundColor: "lightcoral",
                  borderRadius: "3px",
                  border: "none",
                  color: "white",
                }}
                onClick={() => {dispatch(rejectAdminTransaction(transaction_id)); raiseSomethingChanged(true)}}
              >
                <i className="fas fa-times" />
              </button>
            </td>
          </tr>
        );
      })
    );
  };

  return (
    <div className="content-wrapper">
      {/* Content Header (Page header) */}
      <div className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1 className="m-0 text-dark">Transaction List</h1>
            </div>
            {/* /.col */}
          </div>
          {/* /.row */}
        </div>
        {/* /.container-fluid */}
      </div>
      <div className="row">
        <div className="col-md-12">
          <div className="card">
            <div className="card-body p-0">
              <div className="table-responsive">
                <table className="table m-0">
                  <thead>
                    <tr>{renderHeader()}</tr>
                  </thead>
                  <tbody>{renderBody()}</tbody>
                </table>
              </div>
              {/* /.table-responsive */}
            </div>
            {/* /.card-body */}
          </div>
        </div>
      </div>
    </div>
  );
}