import React, {useEffect} from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAdminDashboard } from "../actions/adminActions";

export default function AdminDashboard() {
  const dispatch = useDispatch();
  const selector = (state) => {
    if (state.adminSignin.adminInfo) {
      return state.adminSignin;
    } else {
      return state.adminRegister;
    }
  };
  const adminSignin = useSelector((state) => selector(state));
  const { adminInfo } = adminSignin;  
  
  //apiData is the response you get from:
  //curl --location --request GET "http://3.89.42.7/api/authed/admin/dashboard" --header 'X-UserID: 3'
  /* 
  const apiData = {
    "status": 0,
    "description": "success",
    "data": {
        "count_new_costumer": 8,
        "count_new_merchant": 20,
        "count_new_transaction": 3,
        "count_product": 29
    }
  };
  */

  const adminDashboard = useSelector((state) => state.adminDashboard);
  const { data, loading, error } = adminDashboard;

  useEffect(() => {
    dispatch(getAdminDashboard());
  }, []);

  return (
    <div className="content-wrapper">
      {/* Content Header (Page header) */}
      <div className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1 className="m-0 text-dark">Dashboard</h1>
            </div>
            {/* /.col */}
          </div>
          {/* /.row */}
        </div>
        {/* /.container-fluid */}
      </div>
      {/* /.content-header */}
      {/* Main content */}
      <section className="content">
        <div className="container-fluid">
          {/* Info boxes */}
          <div className="row">
            <div className="col-12 col-sm-6 col-md-3">
              <div className="info-box">
                <span className="info-box-icon bg-info elevation-1">
                  <i className="fas fa-user" />
                </span>
                <div className="info-box-content">
                  <span className="info-box-text">New Customer</span>
                  <span className="info-box-number"> {data.count_new_costumer} </span>
                </div>
                {/* /.info-box-content */}
              </div>
              {/* /.info-box */}
            </div>
            {/* /.col */}
            <div className="col-12 col-sm-6 col-md-3">
              <div className="info-box mb-3">
                <span className="info-box-icon bg-danger elevation-1">
                  <i className="fas fa-users" />
                </span>
                <div className="info-box-content">
                  <span className="info-box-text">New Merchant</span>
                  <span className="info-box-number"> {data.count_new_merchant} </span>
                </div>
                {/* /.info-box-content */}
              </div>
              {/* /.info-box */}
            </div>
            {/* /.col */}
            {/* fix for small devices only */}
            <div className="clearfix hidden-md-up" />
            <div className="col-12 col-sm-6 col-md-3">
              <div className="info-box mb-3">
                <span className="info-box-icon bg-success elevation-1">
                  <i className="fas fa-credit-card" />
                </span>
                <div className="info-box-content">
                  <span className="info-box-text">New Transaction</span>
                  <span className="info-box-number">{data.count_new_transaction}</span>
                </div>
                {/* /.info-box-content */}
              </div>
              {/* /.info-box */}
            </div>
            {/* /.col */}
            <div className="col-12 col-sm-6 col-md-3">
              <div className="info-box mb-3">
                <span className="info-box-icon bg-warning elevation-1">
                  <i className="fas fa-shopping-cart" />
                </span>
                <div className="info-box-content">
                  <span className="info-box-text">New Product</span>
                  <span className="info-box-number"> {data.count_product}</span>
                </div>
                {/* /.info-box-content */}
              </div>
              {/* /.info-box */}
            </div>
            {/* /.col */}
          </div>
        </div>
        {/*/. container-fluid */}
      </section>
      {/* /.content */}
    </div>
  );
}
