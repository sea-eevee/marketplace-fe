import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getAdminProposal, acceptAdminProposal, rejectAdminProposal } from "../actions/adminActions";

export default function AdminProposal() {
  const dispatch = useDispatch();
  //somethingChanged is raised when you click confirmation buttons
  const [somethingChanged, raiseSomethingChanged] = useState(false);
  const selector = (state) => {
    if (state.adminSignin.adminInfo) {
      return state.adminSignin;
    } else {
      return state.adminRegister;
    }
  };

  const adminSignin = useSelector((state) => selector(state));
  const { adminInfo } = adminSignin;
 
  //apiData is the response you get from:
  //curl --location --request GET "http://3.89.42.7/api/authed/admin/merchant" --header 'X-UserID: 3'
  /*
  const apiData = {
    "status": 0,
    "description": "success",
    "data": {
        "merchant_proposal": [{
            "merchant_id": 1,
            "username": "john.doe.merchant",
            "email": "ferdian.ifkarsyah@gmail.com",
            "is_approved": null
        }, {
            "merchant_id": 2,
            "username": "samsung.merchant",
            "email": "samsung.merchant@gmail.com",
            "is_approved": null
        }, {
            "merchant_id": 3,
            "username": "apple.merchant",
            "email": "apple.merchant@gmail.com",
            "is_approved": null
        }]
    }
};
*/

  const adminProposal = useSelector((state) => state.adminProposal);
  const { data, loading, error } = adminProposal;

  useEffect(() => {
    dispatch(getAdminProposal());
    raiseSomethingChanged(false);
  }, [somethingChanged]);

  const renderHeader = () => {
    let headerElement = ["MerchantID", "Name", "Email", "Confirmation"];

    return headerElement.map((key, index) => {
      return <th key={index}>{key}</th>;
    });
  };

  const renderBody = () => {
    return (
      data && data.merchant_proposal.map(({ merchant_id, username, email, is_approved }) => {
        return (
          <tr key={merchant_id}>
            <td>{merchant_id}</td>
            <td>{username}</td>
            <td>{email}</td>
            <td>
              <button
                className="btn"
                style={{
                  backgroundColor: "lightgreen",
                  borderRadius: "3px",
                  border: "none",
                  color: "white",
                  marginLeft: "2px",
                  marginRight: "18px",
                }}
                onClick={() => {dispatch(acceptAdminProposal(merchant_id)); raiseSomethingChanged(true)}}
              >
                <i className="fas fa-check" />
              </button>

              <button
                className="btn"
                style={{
                  backgroundColor: "lightcoral",
                  borderRadius: "3px",
                  border: "none",
                  color: "white",
                }}
                onClick={() => {dispatch(rejectAdminProposal(merchant_id)); raiseSomethingChanged(true)}}
              >
                <i className="fas fa-times" />
              </button>
            </td>
          </tr>
        );
      })
    );
  };

  return (
    <div className="content-wrapper">
      {/* Content Header (Page header) */}
      <div className="content-header">
        <div className="container-fluid">
          <div className="row mb-2">
            <div className="col-sm-6">
              <h1 className="m-0 text-dark">Merchant Proposal</h1>
            </div>
            {/* /.col */}
          </div>
          {/* /.row */}
        </div>
        {/* /.container-fluid */}
      </div>
      <div className="row">
        <div className="col-md-12">
          <div className="card">
            <div className="card-body p-0">
              <div className="table-responsive">
                <table className="table m-0">
                  <thead>
                    <tr>{renderHeader()}</tr>
                  </thead>
                  <tbody>
                    {renderBody()}
                  </tbody>
                </table>
              </div>
              {/* /.table-responsive */}
            </div>
            {/* /.card-body */}
          </div>
        </div>
      </div>
    </div>
  );
}
