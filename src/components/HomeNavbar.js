import React from "react";
import { Navbar, Nav, Button } from "react-bootstrap";

function HomeNavbar(props) {
  return (
    <Navbar fixed="top" bg="dark" variant="dark" expand="md">
      <Navbar.Brand href="/">Eeeve Marketplace</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link href="/">Home</Nav.Link>
        </Nav>
        <Nav className="d-flex align-items-center mr-5">
          <Nav.Link href="/customer/register">
          <Button variant="outline-secondary">Register</Button></Nav.Link>
          <Nav.Link href="/customer/login">
            <Button variant="info">Login</Button>
          </Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}
export default HomeNavbar;
