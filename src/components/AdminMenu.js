import React from "react";
import Eevee from "../images/eevee-admin.svg";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";

export default function AdminMenu() {
  const selector = (state) => {
    if (state.adminSignin.adminInfo) {
      return state.adminSignin;
    } else {
      return state.adminRegister;
    }
  };

  const adminSignin = useSelector((state) => selector(state));
  const { adminInfo } = adminSignin;

  return (
    <aside className="main-sidebar sidebar-light-primary elevation-4">
      {/* Sidebar */}
      <div className="sidebar">
        {/* Sidebar user panel (optional) */}
        <div className="user-panel mt-3 pb-3 mb-3 d-flex">
          <div className="image">
            <img src={Eevee} className="img-circle elevation-2" alt="Admin"/>
          </div>
          <div className="info">
              {
                (adminInfo !== undefined) 
                ? <h6> {adminInfo.username} </h6> 
                : <h6> Admin </h6>
              } 
          </div>
        </div>
        {/* Sidebar Menu */}
        <nav className="mt-2">
          <ul
            className="nav nav-pills nav-sidebar flex-column"
            data-widget="treeview"
            role="menu"
            data-accordion="false"
          >
            {/* Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library */}
            <li className="nav-item">
                <Link to="/admin/dashboard" className="nav-link">
                    <i className="nav-icon fas fa-tachometer-alt" />
                    <p>
                        Dashboard
                    </p>
                </Link>
            </li>
            <li className="nav-item">
                <Link to="/admin/proposal" className="nav-link">
                    <i className="nav-icon fas fa-users" />
                    <p>
                        Merchant Proposal
                    </p>
                </Link>
            </li>
            <li className="nav-item">
                    <Link to="/admin/transaction" className="nav-link">
                    <i className="nav-icon fas fa-list-alt" />
                    <p>
                        Transaction List
                    </p>
                    </Link>
            </li>
          </ul>
        </nav>
        {/* /.sidebar-menu */}
      </div>
      {/* /.sidebar */}
    </aside>
  );
}
