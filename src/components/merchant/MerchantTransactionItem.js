import React from "react";
import { Card } from "react-bootstrap";

function MerchantTransactionItem(props) {
  return (
    <Card border="secondary" className="my-3">
      <Card.Body>
        <Card.Link
          href="/customer/merchant"
          style={{ textDecoration: "none", color: "black" }}
        >
          <div className="d-flex justify-content-between">
            <div className="font-weight-bolder">Transaction ID : {props.item.id}</div>
            <div className="text-secondary font-weight-bolder">{props.item.date}</div>
          </div>
        </Card.Link>
        <div className="border-bottom border-secondary mb-3"></div>
        <div className="d-flex">
          <Card.Link
            href="/customer/item"
            className="flex-fill"
            style={{ textDecoration: "none", color: "black" }}
          >
            <div className="ml-3 mb-5">
              <div
                className="font-weight-bolder"
                style={{ fontSize: "1.2rem" }}
              >
                {props.item.name}
              </div>
              <div className="text-success font-weight-bolder">
                Rp. {props.item.price.toLocaleString("id")}
              </div>
              <div>Quantity : {props.item.quantity}</div>
            </div>
          </Card.Link>
          <div
            className="d-flex flex-column justify-content-end"
            style={{ width: "10rem" }}
          >
            <div className="d-flex flex-column">
              <div className="text-secondary">Total Price</div>
              <div
                className="text-success font-weight-bolder"
                style={{ fontSize: "1.2rem" }}
              >
                Rp. {props.item.price * props.item.quantity}
              </div>
            </div>
          </div>
        </div>
      </Card.Body>
    </Card>
  );
}

export default MerchantTransactionItem;
