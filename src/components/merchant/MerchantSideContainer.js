import React from "react";
import SideProfile from "../SideProfile";
import MerchantSideNav from "./MerchantSideNav";

function MerchantSideContainer(props){
  return(
    <SideProfile data={props.data}>
    <div className="d-flex justify-content-between mb-5">
      <div className="text-secondary font-weight-bolder">Wallet</div>
      <div className="text-success font-weight-bolder">
        Rp. 2.000.000
      </div>
    </div>
    <MerchantSideNav defaultActiveKey={props.navActiveKey} />
    {props.children}
  </SideProfile>
  );
}
export default MerchantSideContainer;