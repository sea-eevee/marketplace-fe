import React from "react";
import Cookie from "js-cookie";
import { Nav, Navbar, NavDropdown } from "react-bootstrap";

function MerchantNavbar(props) {
  const logOutClick = () => {
    Cookie.remove("merchantInfo");
    window.location.reload();
  };

  return (
    <Navbar fixed="top" bg="dark" variant="dark" expand="md">
      <Navbar.Brand href="/merchant">Eeeve Marketplace</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link href="/merchant">Home</Nav.Link>
        </Nav>
        <Nav className="d-flex align-items-center mr-5 px-5">
          <NavDropdown title={props.merchantName} id="basic-nav-dropdown">
            <NavDropdown.Item href="/merchant/profile">
              Profile
            </NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item href="#" onClick={logOutClick}>
              Log Out
            </NavDropdown.Item>
          </NavDropdown>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}

export default MerchantNavbar;
