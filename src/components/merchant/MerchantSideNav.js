import React from "react";
import { Nav } from "react-bootstrap";

function MerchantSideNav(props) {
  return (
    <Nav
      variant="pills"
      defaultActiveKey={props.defaultActiveKey}
      className="d-flex flex-column"
    >
      <Nav.Item>
        <Nav.Link href="/merchant/profile" eventKey="link-1">
          Profile
        </Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link eventKey="link-2">Transfer History</Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link href="/merchant" eventKey="link-3">List Items</Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link eventKey="link-4">List Request Items</Nav.Link>
      </Nav.Item>
    </Nav>
  );
}
export default MerchantSideNav;
