import React from "react";
import { Button, Card } from "react-bootstrap";

function MerchantCrudItem(props) {
  return (
    <Card className="my-3">
      <Card.Body>
        <Card.Link
          style={{ textDecoration: "none", color: "black" }}
        >
          <div className="d-flex justify-content-between">
            <div className="font-weight-bolder">
              Product ID : {props.item.ItemID}
            </div>
          </div>
        </Card.Link>
        <div className="border-bottom border-secondary mb-3"></div>
        <div className="d-flex">
          <Card.Link
            className="flex-fill"
            style={{ textDecoration: "none", color: "black" }}
          >
            <div className="ml-3 mb-5">
              <div
                className="font-weight-bolder"
                style={{ fontSize: "1.2rem" }}
              >
                {props.item.DisplayName}
              </div>
              <div className="text-success font-weight-bolder">
                Rp. {props.item.Price.toLocaleString("id")}
              </div>
              <div>Stock : {props.item.quantity}</div>
              <div className="mt-3">Description: {props.item.Description}</div>
            </div>
          </Card.Link>
          <div
            className="d-flex flex-column justify-content-around align-items-center border-left border-secondary"
            style={{ minWidth: "25%" }}
          >
            <Button
              variant="info"
              size="sm"
              style={{ width: "60%" }}
              className="font-weight-bolder"
            >
              Update
            </Button>
            <Button
              variant="danger"
              size="sm"
              style={{ width: "60%" }}
              className="font-weight-bolder"
            >
              Delete
            </Button>
          </div>
        </div>
      </Card.Body>
    </Card>
  );
}
export default MerchantCrudItem;
