import React from "react";
import { Nav } from "react-bootstrap";

function MerchantProfileTab(props) {
  return (
    <Nav fill justify variant="tabs" defaultActiveKey={props.defaultActiveKey}>
      <Nav.Item>
        <Nav.Link href="/merchant/profile" eventKey="link-1">
          Edit Profile
        </Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link href="/merchant/history" eventKey="link-2">
          Transaction History
        </Nav.Link>
      </Nav.Item>
    </Nav>
  );
}
export default MerchantProfileTab;
