import React from "react";
import { Link } from "react-router-dom";
import Cookie from "js-cookie";
import {useSelector} from "react-redux";
import { propTypes } from "react-bootstrap/esm/Image";

function AdminHeader(props) {
  const logout = () => {
    Cookie.remove("adminInfo");
    window.location.reload();
  }

  return (
    <nav className="main-header navbar navbar-expand navbar-white navbar-light">
      {/* Right Navbar Menu */}
      <ul className="navbar-nav ml-auto">
        <li className="nav-item">
          <button
            className="btn btn-white"
            data-widget="pushmenu"
            data-slide="true"
          >
            <i className="fas fa-bars" />
          </button>
            <button onClick = {logout}
              className="btn btn-white">
              <i className="fa fa-power-off" />
            </button>
        </li>
      </ul>
    </nav>
  );
}
export default AdminHeader;
