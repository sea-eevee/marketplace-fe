import Axios from "axios";
import Cookie from "js-cookie";
import {
  MERCHANT_SIGNIN_REQUEST,
  MERCHANT_SIGNIN_SUCCESS,
  MERCHANT_SIGNIN_FAIL,
  MERCHANT_REGISTER_REQUEST,
  MERCHANT_REGISTER_SUCCESS,
  MERCHANT_REGISTER_FAIL, MERCHANT_LIST_ITEM_REQUEST, MERCHANT_LIST_ITEM_SUCCESS, MERCHANT_ADD_ITEM_REQUEST, MERCHANT_ADD_ITEM_SUCCESS, MERCHANT_ADD_ITEM_FAIL
} from "../constants/merchantConstants";

const signin = (name, password) => async (dispatch) => {
  dispatch({ type: MERCHANT_SIGNIN_REQUEST, payload: {} });
  try {
    const { data } = await Axios.post(
      "http://3.89.42.7/api/sso/login",
      JSON.stringify({ username: name, password: password, role: "merchant" })
    );
    if (data.status === 0) {
      dispatch({ type: MERCHANT_SIGNIN_SUCCESS, payload: data.data });
      Cookie.set("merchantInfo", JSON.stringify(data.data));
    } else {
      dispatch({ type: MERCHANT_SIGNIN_FAIL, payload: data.description });
    }
  } catch (error) {
    dispatch({ type: MERCHANT_SIGNIN_FAIL, payload: error.message });
  }
};

const register = (name, email, password) => async (dispatch) => {
  dispatch({ type: MERCHANT_REGISTER_REQUEST, payload: {} });
  try {
    const { data } = await Axios.post(
      "http://3.89.42.7/api/sso/register",
      JSON.stringify({
        username: name,
        password: password,
        role: "merchant",
        email: email,
      })
    );
    if (data.status === 0) {
      dispatch({ type: MERCHANT_REGISTER_SUCCESS, payload: data.data });
      Cookie.set("merchantInfo", JSON.stringify(data.data));
    } else {
      dispatch({ type: MERCHANT_REGISTER_FAIL, payload: data.description });
    }
  } catch (error) {
    dispatch({ type: MERCHANT_REGISTER_FAIL, payload: error.message });
  }
};

const merchantListItem = (id) => async (dispatch) => {
  dispatch({ type: MERCHANT_LIST_ITEM_REQUEST, payload: {} });
  try {
    const { data } = await Axios.get(
      "http://3.89.42.7/api/authed/merchant/item",
      {
        headers: {
          "X-UserID": id,
        },
      }
    );
    if (data.status === 0) {
      dispatch({ type: MERCHANT_LIST_ITEM_SUCCESS, payload: data.data.items });
    }
  } catch (error) {
    dispatch({ type: MERCHANT_REGISTER_FAIL, payload: error.message });
  }
};

const merchantAddItem = (id, item) => async (dispatch) => {
  const form_data = new FormData();
  for(var key in item){
    form_data.append(key, item[key]);
  }
  dispatch({ type: MERCHANT_ADD_ITEM_REQUEST, payload: {} });
  try {
    const { data } = await Axios.post(
      "http://3.89.42.7/api/authed/merchant/item", form_data,
      {
        headers: {
          "X-UserID": id,
        },
      }
    );
    if (data.status === 0) {
      dispatch({ type: MERCHANT_ADD_ITEM_SUCCESS, payload: data.data.description});
    }
  } catch (error) {
    dispatch({ type: MERCHANT_ADD_ITEM_FAIL, payload: error.message });
  }
};

export { signin, register, merchantListItem, merchantAddItem };
