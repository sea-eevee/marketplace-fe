import Axios from "axios";
import Cookie from "js-cookie";
import sha256 from "js-sha256";
import {
  ADMIN_SIGNIN_REQUEST,
  ADMIN_SIGNIN_SUCCESS,
  ADMIN_SIGNIN_FAIL,
  ADMIN_REGISTER_REQUEST,
  ADMIN_REGISTER_SUCCESS,
  ADMIN_REGISTER_FAIL,
  ADMIN_DASHBOARD_REQUEST,
  ADMIN_DASHBOARD_SUCCESS,
  ADMIN_DASHBOARD_FAIL,
  ADMIN_PROPOSAL_REQUEST,
  ADMIN_PROPOSAL_SUCCESS,
  ADMIN_PROPOSAL_FAIL,
  ADMIN_ACCEPTPROPOSAL_REQUEST,
  ADMIN_ACCEPTPROPOSAL_SUCCESS,
  ADMIN_ACCEPTPROPOSAL_FAIL,
  ADMIN_REJECTPROPOSAL_REQUEST,
  ADMIN_REJECTPROPOSAL_SUCCESS,
  ADMIN_REJECTPROPOSAL_FAIL,
  ADMIN_TRANSACTION_REQUEST,
  ADMIN_TRANSACTION_SUCCESS,
  ADMIN_TRANSACTION_FAIL,
  ADMIN_ACCEPTTRANSACTION_REQUEST,
  ADMIN_ACCEPTTRANSACTION_SUCCESS,
  ADMIN_ACCEPTTRANSACTION_FAIL,
  ADMIN_REJECTTRANSACTION_REQUEST,
  ADMIN_REJECTTRANSACTION_SUCCESS,
  ADMIN_REJECTTRANSACTION_FAIL
} from "../constants/adminConstants";

const signin = (name, password) => async (dispatch) => {
  dispatch({ type: ADMIN_SIGNIN_REQUEST, payload: {} });
  try {
    const { data } = await Axios.post("http://3.89.42.7/api/sso/login", 
    JSON.stringify({ username: name, password: (password), role: "admin" }));
    if (data.status === 0) {
      dispatch({ type: ADMIN_SIGNIN_SUCCESS, payload: data.data });
      Cookie.set("adminInfo", JSON.stringify(data.data));
    } else {
      dispatch({ type: ADMIN_SIGNIN_FAIL, payload: data.description });
    }
  } catch (error) {
    dispatch({ type: ADMIN_SIGNIN_FAIL, payload: error.message });
  }
};

const register = (name, password, token) => async (dispatch) => {
  dispatch({ type: ADMIN_REGISTER_REQUEST, payload: { } });
  try {
    const { data } = await Axios.post(
      "http://3.89.42.7/api/sso/register",
      JSON.stringify({ admin_token: token, username: name, password: (password), role: "admin" })
    );
    if (data.status === 0) {
      dispatch({ type: ADMIN_REGISTER_SUCCESS, payload: data.data });
      Cookie.set("adminInfo", JSON.stringify(data.data));
    } else {
      dispatch({ type: ADMIN_REGISTER_FAIL, payload: data.description });
    }
  } catch (error) {
    dispatch({ type: ADMIN_REGISTER_FAIL, payload: error.message });
  }
};

const getAdminDashboard = () => async (dispatch) => {
  dispatch({ type: ADMIN_DASHBOARD_REQUEST, payload: {} });
  try {
    const { data } = await Axios.get(
      "http://3.89.42.7/api/authed/admin/dashboard"
    );
    if (data.status === 0) {
      console.log(data);
      dispatch({ type: ADMIN_DASHBOARD_SUCCESS, payload: data.data });
    }
  } catch (error) {
    dispatch({ type: ADMIN_DASHBOARD_FAIL, payload: error.message });
  }
};

const getAdminProposal = () => async (dispatch) => {
  dispatch({ type: ADMIN_PROPOSAL_REQUEST, payload: {} });
  try {
    const { data } = await Axios.get(
      "http://3.89.42.7/api/authed/admin/merchant"
    );
    if (data.status === 0) {
      console.log(data);
      dispatch({ type: ADMIN_PROPOSAL_SUCCESS, payload: data.data });
    }
  } catch (error) {
    dispatch({ type: ADMIN_PROPOSAL_FAIL, payload: error.message });
  }
};

const acceptAdminProposal = (merchant_id) => async (dispatch) => {
  dispatch({ type: ADMIN_ACCEPTPROPOSAL_REQUEST, payload: {} });
  try {
    const { data } = await Axios.put(
      `http://3.89.42.7/api/authed/admin/merchant/${merchant_id}`,
      {"is_approved": true}
    );
    if (data.status === 0) {
      console.log(data);
      dispatch({ type: ADMIN_ACCEPTPROPOSAL_SUCCESS, payload: data.data });
    }
  } catch (error) {
    dispatch({ type: ADMIN_ACCEPTPROPOSAL_FAIL, payload: error.message });
  }
};

const rejectAdminProposal = (merchant_id) => async (dispatch) => {
  dispatch({ type: ADMIN_REJECTPROPOSAL_REQUEST, payload: {} });
  try {
    const { data } = await Axios.put(
      `http://3.89.42.7/api/authed/admin/merchant/${merchant_id}`,
      {"is_approved": false}
    );
    if (data.status === 0) {
      console.log(data);
      dispatch({ type: ADMIN_REJECTPROPOSAL_SUCCESS, payload: data.data });
    }
  } catch (error) {
    dispatch({ type: ADMIN_REJECTPROPOSAL_FAIL, payload: error.message });
  }
};

const getAdminTransaction = () => async (dispatch) => {
  dispatch({ type: ADMIN_TRANSACTION_REQUEST, payload: {} });
  try {
    const { data } = await Axios.get(
      "http://3.89.42.7/api/authed/admin/transaction"
    );
    if (data.status === 0) {
      console.log(data);
      dispatch({ type: ADMIN_TRANSACTION_SUCCESS, payload: data.data });
    }
  } catch (error) {
    dispatch({ type: ADMIN_TRANSACTION_FAIL, payload: error.message });
  }
};

const acceptAdminTransaction = (transaction_id) => async (dispatch) => {
  dispatch({ type: ADMIN_ACCEPTTRANSACTION_REQUEST, payload: {} });
  try {
    const { data } = await Axios.put(
      `http://3.89.42.7/api/authed/admin/transaction/${transaction_id}/accept`
    );
    if (data.status === 0) {
      console.log(data);
      dispatch({ type: ADMIN_ACCEPTTRANSACTION_SUCCESS, payload: data.data });
    }
  } catch (error) {
    dispatch({ type: ADMIN_ACCEPTTRANSACTION_FAIL, payload: error.message });
  }
};

const rejectAdminTransaction = (transaction_id) => async (dispatch) => {
  dispatch({ type: ADMIN_REJECTTRANSACTION_REQUEST, payload: {} });
  try {
    const { data } = await Axios.put(
      `http://3.89.42.7/api/authed/admin/merchant/${transaction_id}/reject`
    );
    if (data.status === 0) {
      console.log(data);
      dispatch({ type: ADMIN_REJECTTRANSACTION_SUCCESS, payload: data.data });
    }
  } catch (error) {
    dispatch({ type: ADMIN_REJECTTRANSACTION_FAIL, payload: error.message });
  }
};

export { signin, register, getAdminDashboard, getAdminProposal, acceptAdminProposal, rejectAdminProposal, getAdminTransaction, acceptAdminTransaction, rejectAdminTransaction };
