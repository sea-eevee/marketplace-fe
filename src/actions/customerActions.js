import Axios from "axios";
import Cookie from "js-cookie";
import sha256 from "js-sha256";
import {
  USER_SIGNIN_REQUEST,
  USER_SIGNIN_SUCCESS,
  USER_SIGNIN_FAIL,
  USER_REGISTER_REQUEST,
  USER_REGISTER_SUCCESS,
  USER_REGISTER_FAIL,
  USER_LIST_MERCHANT_REQUEST,
  USER_LIST_MERCHANT_SUCCESS,
  USER_LIST_MERCHANT_FAIL,
  USER_DETAIL_MERCHANT_REQUEST,
  USER_DETAIL_MERCHANT_SUCCESS,
  USER_DETAIL_MERCHANT_FAIL,
  USER_PROFILE_REQUEST,
  USER_PROFILE_SUCCESS,
  USER_PROFILE_FAIL,
  USER_READ_CART_REQUEST,
  USER_READ_CART_SUCCESS,
  USER_READ_CART_FAIL,
  USER_ADD_CART_REQUEST,
  USER_ADD_CART_SUCCESS,
  USER_ADD_CART_FAIL,
  USER_PROFILE_UPDATE_REQUEST,
  USER_PROFILE_UPDATE_SUCCESS,
  USER_PROFILE_UPDATE_FAIL,
  CART_QUANTITY_EDIT_REQUEST,
  CART_QUANTITY_EDIT_SUCCESS,
  CART_QUANTITY_EDIT_FAIL,
  USER_READ_TRANSACTION_REQUEST,
  USER_READ_TRANSACTION_SUCCESS,
  USER_READ_TRANSACTION_FAIL,
  USER_LIST_ORDER_REQUEST,
  USER_LIST_ORDER_SUCCESS,
  USER_LIST_ORDER_FAIL,
  USER_PAID_ORDER_REQUEST,
  USER_PAID_ORDER_SUCCESS,
  USER_PAID_ORDER_FAIL, USER_CART_ORDER_REQUEST, USER_CART_ORDER_SUCCESS, USER_CART_ORDER_FAIL
} from "../constants/customerConstants";

const signin = (name, password) => async (dispatch) => {
  dispatch({ type: USER_SIGNIN_REQUEST, payload: {} });
  try {
    const { data } = await Axios.post(
      "http://3.89.42.7/api/sso/login",
      JSON.stringify({
        username: name,
        password: sha256(password),
        role: "customer",
      })
    );
    if (data.status === 0) {
      dispatch({ type: USER_SIGNIN_SUCCESS, payload: data.data });
      Cookie.set("userInfo", JSON.stringify(data.data));
    } else {
      dispatch({ type: USER_SIGNIN_FAIL, payload: data.description });
    }
  } catch (error) {
    dispatch({ type: USER_SIGNIN_FAIL, payload: error.message });
  }
};

const register = (name, email, password) => async (dispatch) => {
  dispatch({ type: USER_REGISTER_REQUEST, payload: {} });
  try {
    const { data } = await Axios.post(
      "http://3.89.42.7/api/sso/register",
      JSON.stringify({
        username: name,
        password: sha256(password),
        role: "customer",
        email: email,
      })
    );
    if (data.status === 0) {
      dispatch({ type: USER_REGISTER_SUCCESS, payload: data.data });
      Cookie.set("userInfo", JSON.stringify(data.data));
    } else {
      dispatch({ type: USER_REGISTER_FAIL, payload: data.description });
    }
  } catch (error) {
    dispatch({ type: USER_REGISTER_FAIL, payload: error.message });
  }
};

const userAddCart = (user_id, item) => async (dispatch) => {
  dispatch({ type: USER_ADD_CART_REQUEST, payload: {} });
  try {
    const { data } = await Axios.post(
      "http://3.89.42.7/api/authed/customer/cart/",
      JSON.stringify({
        item_id: item.id,
        quantity: item.quantity,
      }),
      {
        headers: {
          "X-UserID": user_id,
        },
      }
    );
    if (data.status === 0) {
      dispatch({ type: USER_ADD_CART_SUCCESS, payload: data.data });
    }
  } catch (error) {
    dispatch({ type: USER_ADD_CART_FAIL, payload: error.message });
  }
};

const userReadCart = (id) => async (dispatch) => {
  dispatch({ type: USER_READ_CART_REQUEST, payload: {} });
  try {
    const { data } = await Axios.get(
      "http://3.89.42.7/api/authed/customer/cart/",
      {
        headers: {
          "X-UserID": id,
        },
      }
    );
    if (data.status === 0) {
      dispatch({ type: USER_READ_CART_SUCCESS, payload: data.data.items });
    }
  } catch (error) {
    dispatch({ type: USER_READ_CART_FAIL, payload: error.message });
  }
};

const userReadTransaction = (id) => async (dispatch) => {
  dispatch({ type: USER_READ_TRANSACTION_REQUEST, payload: {} });
  try {
    const { data } = await Axios.get(
      "http://3.89.42.7/api/authed/customer/transaction/",
      {
        headers: {
          "X-UserID": id,
        },
      }
    );
    if (data.status === 0) {
      dispatch({
        type: USER_READ_TRANSACTION_SUCCESS,
        payload: data.data.transactions ? data.data.transactions : [],
      });
    }
  } catch (error) {
    dispatch({ type: USER_READ_TRANSACTION_FAIL, payload: error.message });
  }
};

const cartEditQuantity = (user_id, item_id, add_one) => async (dispatch) => {
  dispatch({ type: CART_QUANTITY_EDIT_REQUEST, payload: {} });
  try {
    const { data } = await Axios.put(
      "http://3.89.42.7/api/authed/customer/cart/",
      JSON.stringify({
        item_id: item_id,
        is_add_one: add_one,
      }),
      {
        headers: {
          "X-UserID": user_id,
        },
      }
    );
    if (data.status === 0) {
      dispatch({ type: CART_QUANTITY_EDIT_SUCCESS, payload: data.description });
    }
  } catch (error) {
    dispatch({ type: CART_QUANTITY_EDIT_FAIL, payload: error.message });
  }
};

const getUserProfile = (id) => async (dispatch) => {
  dispatch({ type: USER_PROFILE_REQUEST, payload: {} });
  try {
    const { data } = await Axios.get(
      "http://3.89.42.7/api/authed/customer/profile/",
      {
        headers: {
          "X-UserID": id,
        },
      }
    );
    if (data.status === 0) {
      dispatch({ type: USER_PROFILE_SUCCESS, payload: data.data });
    }
  } catch (error) {
    dispatch({ type: USER_PROFILE_FAIL, payload: error.message });
  }
};

const updateUserProfile = (id, name, address) => async (dispatch) => {
  dispatch({ type: USER_PROFILE_UPDATE_REQUEST, payload: {} });
  try {
    const { data } = await Axios.put(
      "http://3.89.42.7/api/authed/customer/profile/",

      JSON.stringify({
        display_name: name,
        address: address,
      }),
      {
        headers: {
          "X-UserID": id,
        },
      }
    );
    if (data.status === 0) {
      dispatch({ type: USER_PROFILE_UPDATE_SUCCESS, payload: data.data });
    }
  } catch (error) {
    dispatch({ type: USER_PROFILE_UPDATE_FAIL, payload: error.message });
  }
};

const getlistMerchant = () => async (dispatch) => {
  dispatch({ type: USER_LIST_MERCHANT_REQUEST, payload: {} });
  try {
    const { data } = await Axios.get(
      "http://3.89.42.7/api/authed/customer/merchant/"
    );
    if (data.status === 0) {
      dispatch({ type: USER_LIST_MERCHANT_SUCCESS, payload: data.data });
    }
  } catch (error) {
    dispatch({ type: USER_LIST_MERCHANT_FAIL, payload: error.message });
  }
};

const getDetailMerchant = (id) => async (dispatch) => {
  dispatch({ type: USER_DETAIL_MERCHANT_REQUEST, payload: {} });
  try {
    const { data } = await Axios.get(
      `http://3.89.42.7/api/authed/customer/merchant/${id}`
    );
    if (data.status === 0) {
      dispatch({ type: USER_DETAIL_MERCHANT_SUCCESS, payload: data.data });
    }
  } catch (error) {
    dispatch({ type: USER_DETAIL_MERCHANT_FAIL, payload: error.message });
  }
};

const userCartOrder = (id) => async (dispatch) => {
  dispatch({ type: USER_CART_ORDER_REQUEST, payload: {} });
  try {
    const { data } = await Axios.post(
      "http://3.89.42.7/api/authed/customer/order/",
      {}
      ,
      {
        headers: {
          "X-UserID": id,
        },
      }
    );
    if (data.status === 0) {
      dispatch({
        type: USER_CART_ORDER_SUCCESS,
        payload: data.data.description ,
      });
    }
  } catch (error) {
    dispatch({ type: USER_CART_ORDER_FAIL, payload: error.message });
  }
};

const getUserListOrder = (id) => async (dispatch) => {
  dispatch({ type: USER_LIST_ORDER_REQUEST, payload: {} });
  try {
    const { data } = await Axios.get(
      "http://3.89.42.7/api/authed/customer/order/",
      {
        headers: {
          "X-UserID": id,
        },
      }
    );
    if (data.status === 0) {
      dispatch({
        type: USER_LIST_ORDER_SUCCESS,
        payload: data.data.customer_order ? data.data.customer_order : [],
      });
    }
  } catch (error) {
    dispatch({ type: USER_LIST_ORDER_FAIL, payload: error.message });
  }
};

const userPaidOrder = (id, order) => async (dispatch) => {
  dispatch({ type: USER_PAID_ORDER_REQUEST, payload: {} });
  try {
    const { data } = await Axios.post(
      "http://3.89.42.7/api/authed/customer/transaction/",
      JSON.stringify({
        order_id: order.id,
        bank_name: order.bank_name,
        bank_account_number: order.bank_number
      }),
      {
        headers: {
          "X-UserID": id,
        },
      }
    );
    if (data.status === 0) {
      dispatch({
        type: USER_PAID_ORDER_SUCCESS,
        payload: data.data.description ,
      });
    }
  } catch (error) {
    dispatch({ type: USER_PAID_ORDER_FAIL, payload: error.message });
  }
};

export {
  signin,
  register,
  getUserProfile,
  updateUserProfile,
  getlistMerchant,
  getDetailMerchant,
  userReadCart,
  userAddCart,
  cartEditQuantity,
  userReadTransaction,
  getUserListOrder,
  userPaidOrder,
  userCartOrder
};
