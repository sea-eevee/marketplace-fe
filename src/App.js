import React from "react";
import "./App.css";
import { BrowserRouter, Route } from "react-router-dom";
import MerchantScreen from "./screens/merchant/MerchantScreen";
import AdminScreen from "./screens/admin/AdminScreen";
import CustomerScreen from "./screens/customer/CustomerScreen";

function App() {
  return (
    <BrowserRouter>
      <Route path="/" component={CustomerScreen} />
      <Route path="/merchant" component={MerchantScreen} />
      <Route path="/admin" component={AdminScreen} />
    </BrowserRouter>
  );
}

export default App;
